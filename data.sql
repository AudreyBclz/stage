 use friterie;
INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Classiques'),
(2, 'Nouveautés'),
(4, 'Fraîcheurs'),
(5, 'Sauce');
INSERT INTO `product` (`id`, `description`, `name`, `price`, `stock`, `id_c`) VALUES
(1, 'sandwich steak + frite', 'Américain steak', 5.5, 13, 1),
(2, 'Sandwich merguez + frites', 'Américain merguez', 5, 6, 1),
(3, 'Sandwich mexicanos + frites', 'Américain mexicanos', 5.5, 7, 1),
(4, 'Sandwich brochette + frites', 'Américain brochette', 5, 7, 1),
(5, 'Sandwich saucisse + frites', 'Américain saucisse', 5, 8, 1),
(6, 'Sandwich fricandelle + frites', 'Américain fricandelle', 5.5, 8, 1),
(7, 'Sandwich fricandelle XXL+ frites', 'Américain fricandelle XXL', 7.5, 7, 1),
(10, 'Sans sauce', 'Sans sauce', 0, 20, 5),
(11, 'Sauce ketchup', 'Ketchup', 0.5, 19, 5),
(13, 'Sauce mayonnaise', 'Mayonnaise', 0.5, 12467, 5);
 INSERT INTO `authority`(`id`,`name`) VALUES
 (1,'ROLE_ADMIN'),
 (2,'ROLE_USER');
 INSERT INTO `address` (`id`, `city`, `street`, `zip`) VALUES
(1, 'Paris', '48 rue des Fleurs', '75000'),
(2, 'Paris', '54 rue des Fleurs', '75000'),
(4, 'Lille', '47 rue des Lilas', '59000'),
(5, 'Béthune', '32 rue des Martyrs', '62400'),
(6, 'Lille', '352 rue du Général de Gaulle', '59000'),
(7, 'Lille Cedex', '352 rue du Général de Gaulle', '59001');
 INSERT INTO `user` (`id`, `email`, `first_name`, `id_au`, `last_name`, `password`, `id_a`) VALUES
(3, 'maggie@email.com', 'Maggie', 1, 'Jones', '$2a$10$JuVdWZZhTElF6XyrqynM2OQ4Jp8HGC0nYzp8JAqqhmWY08XLJqan.', 1),
(8, 'dupont.jean@hotmail.fr', 'Jean', 2, 'Dupont', '$2a$10$FhKCfSoPzs4AQ7dGeEaC/.4tLsB8qFPZxmmUZPKMIaXULQBwDaD7O', 4),
(11, 'audrey.bucholz@hotmail.fr', 'Audrey', 2, 'Monroe', '$2a$10$2Ogpgc8JaXcvaJkYuclbnu9q1xIuavDEMimi74P9UvFDP0poRGpWy', 7);
INSERT INTO `actuality` (`id`, `description`, `is_display`, `title`, `id_admin`) VALUES
(1, 'La Friterie sera ouverte le 11/05/2023', b'1', 'Ouverture Exceptionnelle', 3),
(4, 'La Friterie sera fermée le 01/05/2023', b'1', 'Fermeture Exceptionnelle', 3),
(13, '2 sandwich achetés => une petite frite offerte', b'1', 'Promotions 18/04/2023 seulement', 3),
(14, 'dsdqzdqdzqdz fq df', b'0', 'Test2', 3),
(15, 'Le jour de votre anniversaire, la boisson vous est offerte!', b'1', 'Promotion ', 3),
(17, 'Fermeture le 24/04/2023', b'0', 'Fermeture', 3),
(18, 'Le magasin sera fermée le 27/04/2023', b'1', 'Fermeture exceptionnelle', 3);