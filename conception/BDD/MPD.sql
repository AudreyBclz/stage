create database friterie;
use friterie;
create table address(
id_a int primary key auto_increment,
street_a varchar(200) not null,
zip_a int(5) not null,
city_a varchar(50));

create table authority(
id_au int primary key auto_increment,
name_au varchar(50) not null);

create table user(
id_u int primary key auto_increment,
lastName_u varchar(50) not null,
firstName_u varchar(50) not null,
email_u varchar(50) not null,
password varchar(100),
id_a int,
id_au int,
constraint fk_id_a foreign key (id_a) references address(id_a),
constraint fk_id_au foreign key (id_au) references authority(id_au));

create table orders(
id_o int primary key auto_increment,
date_o datetime not null,
id_u int,
constraint fk_id_user foreign key (id_u) references user(id_u));

create table category(
id_c int primary key auto_increment,
name_c varchar(50) not null,
id_admin int,
constraint fk_id_admin_c foreign key (id_admin) references user(id_u));

create table actuality(
id_ac int primary key auto_increment,
title_ac varchar(50) not null,
description_ac varchar(200) not null,
isDisplay_ac boolean,
id_admin int,
constraint fk_admin_a foreign key (id_admin) references user(id_u));

create table product(
id_p int primary key auto_increment,
name_p varchar(75) not null,
description_p varchar(400),
price_p double,
stock_p int not null,
id_sauce int,
id_admin int,
id_c int,
constraint fk_id_sauce foreign key (id_sauce) references product(id_p),
constraint fk_id_admin_p foreign key (id_admin) references user(id_u),
constraint fk_id_c foreign key (id_c) references category(id_c));

create table orderline(
id_ol int primary key auto_increment,
quantity_ol int not null,
product_ol varchar(75) not null,
price_ol double not null,
id_p int,
id_o int,
constraint fk_id_product foreign key (id_p) references product(id_p),
constraint fk_id_order foreign key (id_o) references orders(id_o));


