import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardAdminService {

  constructor(private route:Router,private storageService:StorageService) { }

  canActivate():boolean{
    this.storageService.isLoggedInAdmin() == false ? this.route.navigate(['']):"";
    return this.storageService.isLoggedInAdmin();
  }
}
