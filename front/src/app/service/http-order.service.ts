import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OrderRequest } from '../model/OrderRequest';

@Injectable({
  providedIn: 'root'
})
export class HttpOrderService {

  constructor(private http:HttpClient) { }
  url:string="http://localhost:8083/order"

  create(orderRequest: OrderRequest){
    return this.http.post(this.url,orderRequest)
  }
  findById(id:number){
    return this.http.get(this.url+"/"+id);
  }
  updateStatus(orderRequest:OrderRequest,id:number){
    return this.http.patch(this.url+"/status/"+id,orderRequest);
  }
  findOrdersByUser(idUser:number){
    return this.http.get(this.url+"/user/"+idUser);
  }
}
