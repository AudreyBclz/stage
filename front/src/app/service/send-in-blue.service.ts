import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SendInBlueService {
  url:string="https://api.sendinblue.com/v3/smtp/email";
  headers:HttpHeaders = new HttpHeaders();
  constructor(private http:HttpClient) { }
  send(mail:string,idN:number){
    let data =JSON.stringify({
    to:[{
      email:mail
    }],
    templateName: "Friterie du Village",
    templateId:3,
    subject: "Réinitialisation du mot de passe",
    params:{
      id:idN
    },
    isActive: true
})
return this.http.post(this.url,data,{headers:this.headers});
  }
}
