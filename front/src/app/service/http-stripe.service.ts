import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cart } from '../model/Cart';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpStripeService {

  url:string="http://localhost:8083/paiement"
  constructor(private http:HttpClient) { }

  payer(productCarts:Cart[],idUser:number,idOrder:number){
    return this.http.post(this.url+"/"+idUser+"/order/"+idOrder,productCarts,{responseType:'text'});
  }
}
