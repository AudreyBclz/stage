import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserRequest } from '../model/UserRequest';

@Injectable({
  providedIn: 'root'
})
export class HttpUserService {

  url : string = "http://localhost:8083/"
  constructor(private http: HttpClient) { }

  login(username:any){
    return this.http.post(this.url+"login",username);
  }
  create(userRequest:UserRequest){
    return this.http.post(this.url+"user",userRequest);
  }
  getUserById(id:number){
    return this.http.get(this.url+"user/"+id);
  }
  updateUserAddressOrMail(user:any,id:number){
    return this.http.put(this.url+"user/"+id,user);
  }
  getAll(){
    return this.http.get(this.url+"user");
  }
  delete(id:number){
    return this.http.delete(this.url+"user/"+id,{responseType:'text'});
  }
  updatePassword(userRequest:UserRequest,id:number){
    return this.http.put(this.url+"user/reset-password/"+id,userRequest);
  }
  getUserByEmail(email:string){
    return this.http.get(this.url+"user/search/"+email);
  }
}
