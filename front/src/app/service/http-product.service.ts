import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ProductRequest } from '../model/ProductRequest';

@Injectable({
  providedIn: 'root'
})
export class HttpProductService {

  constructor(private http: HttpClient) { }
  url:string = "http://localhost:8083/product"

  create(productRequest:ProductRequest){
    return this.http.post(this.url,productRequest)
  }
  getAll(){
    return this.http.get(this.url);
  }
  getOneById(id:number){
    return this.http.get(this.url+"/"+id);
  }
  update(productRequest:ProductRequest,id:number){
    return this.http.put(this.url+"/"+id,productRequest);
  }
  delete(id:number){
    return this.http.delete(this.url+"/"+id,{responseType:'text'});
  }
  getProductByCat(cat:string){
    return this.http.get(this.url+"/category/"+cat);
  }
  getProductAvailable(){
    return this.http.get(this.url+"/dispo");
  }
}
