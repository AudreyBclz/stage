import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CategoryRequest } from '../model/CategoryRequest';

@Injectable({
  providedIn: 'root'
})
export class HttpCategoryService {

  url : string = "http://localhost:8083/category"
  constructor(private http: HttpClient) { }

  create(categoryRequest:CategoryRequest){
    return this.http.post(this.url,categoryRequest);
  }
  getAll(){
    return this.http.get(this.url);
  }
  delete(id:number){
    return this.http.delete(this.url+"/"+id,{responseType: 'text'});
  }
  getById(id:number){
    return this.http.get(this.url+"/"+id);
  }
  update(request:CategoryRequest, id:number){
    return this.http.put(this.url+"/"+id,request);
  }
}
