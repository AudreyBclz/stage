import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private route:Router,private storageService:StorageService) { }

  canActivate():boolean{
    this.storageService.isLoggedIn() == false ? this.route.navigate(['/connexion']):"";
    return this.storageService.isLoggedIn();
  }
}
