import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActuRequest } from '../model/ActuRequest';

@Injectable({
  providedIn: 'root'
})
export class HttpActuService {

  constructor(private http: HttpClient) { }
  url: string ="http://localhost:8083/actuality"

  create(actuRequest:ActuRequest){
    return this.http.post(this.url,actuRequest);
  }
  getAll(){
    return this.http.get(this.url);
  }
  delete(id:number){
    // ajout du responseType text car la suppression ne retourne pas un Json donc erreur de parse à l'arrivée
    return this.http.delete(this.url+"/"+id,{responseType:'text'});
  }
  getById(id:number){
    return this.http.get(this.url+"/"+id);
  }
  updateActu(actuRequest:ActuRequest,id:number){
    return this.http.put(this.url+"/"+id,actuRequest);
  }
  getDisplay(){
    return this.http.get(this.url+"/display");
  }
}
