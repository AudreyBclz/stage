import { EventEmitter, Injectable, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  @Output()
  change:EventEmitter<Boolean> = new EventEmitter();

  constructor() { }
  isItAdminPage(bool:Boolean){
    this.change.emit(bool);
  }
}
