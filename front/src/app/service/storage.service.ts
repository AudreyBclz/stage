import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  public clearStorage(){
    sessionStorage.removeItem("userdetails");
    sessionStorage.removeItem("Authorization");
  }

  public clearCart(){
    sessionStorage.removeItem("cart");
  }

  public saveCart(cart:any){
    sessionStorage.setItem("cart",JSON.stringify(cart));
  }

  public addProductToCart(prodCart:any){
    let cart:any[]=this.getCart();

    if(cart.length==0){
      cart=[]
    }

    console.log(this.isPresent(prodCart.product.id,prodCart.product.sauce.id));

    if(this.isPresent(prodCart.product.id,prodCart.product.sauce.id)){
      for(let prod of cart){
        if(prod.product.id==prodCart.product.id && prod.product.sauce.id==prodCart.product.sauce.id){
          prod.quantity= prod.quantity+prodCart.quantity;
          this.saveCart(cart);
          return;
        }
    }
  }else{
      cart.push(prodCart);
      this.saveCart(cart);
    }

  }

  public supprProductToCart(idProduct:number,idSauce:number){
    const cart = this.getCart();
    if(cart.length>1){
      for(let i=0; i<cart.length;i++){
        if(cart[i].product.id==idProduct && cart[i].product.sauce.id==idSauce){
          cart.splice(i,1);
          this.saveCart(cart);
          return [cart,true];
        }
      }
    }else if(cart.length==1){
      this.clearCart()
      return [[],true];
    }
    return [cart,false];
  }

  public addQuantityToProdCart(idProduct:number,idSauce:number){
    const cart = this.getCart();
    for(let i=0; i<cart.length;i++){
      if(cart[i].product.id==idProduct && cart[i].product.sauce.id==idSauce){
        cart[i].quantity++;
        this.saveCart(cart);
        return cart;
      }
    }
  }

  public removeQuantityToProdCart(idProduct:number,idSauce:number){
    const cart =this.getCart();
    for(let i=0; i<cart.length;i++){
      if(cart[i].product.id==idProduct && cart[i].product.sauce.id==idSauce){
        if(cart[i].quantity>1){
          cart[i].quantity--;
          this.saveCart(cart);
          return cart;
        }else if(cart[i].quantity==1){
          return this.supprProductToCart(idProduct,idSauce)[0]
        }
      }
    }
  }

  public getCart(){
    const cart = sessionStorage.getItem("cart");
    if(cart){
      return JSON.parse(cart);
    }
    return []
  }
  public getTotal(){
    let total = 0;
    const cart = this.getCart();
    if(cart.length>0){
      for(let prod of cart){
        total=total+(prod.product.price*prod.quantity)
      }
    }
    return total;
  }

  public saveUser(user: any){
    sessionStorage.setItem("userdetails", JSON.stringify(user));
  }

  public getUser(){
    const user = sessionStorage.getItem("userdetails");
    if(user){
      return JSON.parse(user);
    }
    return {}
  }

  public saveAuth(authorization : any){
    sessionStorage.setItem("Authorization", authorization);
  }

  public isLoggedIn(): boolean {
    const user = sessionStorage.getItem('userdetails');
    if (user) {
      return true;
    }

    return false;
  }

  public isLoggedInAdmin():boolean {
    const user = sessionStorage.getItem("userdetails");
    if(user){
      if(JSON.parse(user).role == "ROLE_ADMIN"){
        return true;
      }
    }
    return false;
  }

  public isPresent(idProduct:number,idSauce:number){
    let cart: any[]=this.getCart();
    if(cart.length>0){
      for(let prod of cart){
        if(prod.product.id==idProduct && prod.product.sauce.id==idSauce){
          return true;
        }
      }
    }
    return false;
  }


}
