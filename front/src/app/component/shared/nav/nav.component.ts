import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginResponse } from 'src/app/model/LoginResponse';
import { NavService } from 'src/app/service/nav.service';
import { StorageService } from 'src/app/service/storage.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  collapsed = true;
  collapsedDropActus= false;
  collapsedDropProduct = false;
  collapsedDropCategory = false;
  isLog:Boolean= false;
  role:string;
  isAdminPage:Boolean=false;
  isActiveConnexionTab:Boolean=false;
  isActiveAccountTab:Boolean=false;
  isActiveAccueilTab:Boolean=true;
  isActiveActuTab:Boolean=false;
  isActiveMenuTab:Boolean=false;
  isActiveHoraireTab:Boolean=false;
  infoLogin:LoginResponse;
  constructor(private router:Router, private navService: NavService,private storageService:StorageService) { }

  ngOnInit(): void {
    if(sessionStorage.getItem("userdetails")!=null){
      this.isLog=true;
      this.infoLogin=JSON.parse(sessionStorage.getItem("userdetails"));
      this.role=this.infoLogin.role;
      this.navService.change.subscribe(arg => this.isAdminPage = arg);

    }
  }
  toggleCollapsed(){
    this.collapsed = !this.collapsed;
  }
  toggleDropActus(){
    this.collapsedDropActus = !this.collapsedDropActus;
  }
  toggleDropProduct(){
    this.collapsedDropProduct=!this.collapsedDropProduct;
  }
  toggleDropCategory(){
    this.collapsedDropCategory=!this.collapsedDropCategory;
  }
  logout(){
    this.storageService.clearStorage();
    window.location.href="connexion";
  }
  clickToggle(accueil:boolean,actu:boolean,horaire:boolean,menu:boolean,connexion:boolean,compte:boolean){
    this.isActiveConnexionTab=connexion;
    this.isActiveAccueilTab=accueil;
    this.isActiveActuTab=actu;
    this.isActiveHoraireTab=horaire;
    this.isActiveMenuTab=menu;
    this.isActiveAccountTab=compte;
  }



}
