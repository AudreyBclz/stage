import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserRequest } from 'src/app/model/UserRequest';
import { HttpUserService } from 'src/app/service/http-user.service';
import { NavService } from 'src/app/service/nav.service';

@Component({
  selector: 'app-modif-mdp-form',
  templateUrl: './modif-mdp-form.component.html',
  styleUrls: ['./modif-mdp-form.component.css']
})
export class ModifMdpFormComponent implements OnInit {

  message:string="";
  warning:boolean=false;
  form:FormGroup;
  id:number;
  user:any;
  constructor(private navService: NavService,private fb:FormBuilder,private route: ActivatedRoute,private httpUser:HttpUserService) {
    this.form=fb.group({
      email:this.fb.control('',[Validators.required,Validators.pattern("^([a-z._0-9]*)\@([a-z]{2,10})\\.[a-z]{2,4}$")]),
      pass:this.fb.control('',[Validators.required,Validators.pattern("(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}")]),
      confPass:this.fb.control('',[Validators.required,Validators.pattern("(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}")])
    })
  }

  get email(){
    return this.form.get("email");
  }
  get pass(){
    return this.form.get("pass");
  }
  get confPass(){
    return this.form.get("confPass")
  }
  ngOnInit(): void {
    this.navService.isItAdminPage(false);
    this.id=parseInt(this.route.snapshot.params['id']);
    this.httpUser.getUserById(this.id).subscribe({
      next:(res)=>{
        this.user=res
      }
    })
  }
  modifMotDePasse(){
    console.log(this.user.email);

    if(this.form.valid){
      if(this.form.value.pass !== this.form.value.confPass){
        this.message="Le mot de passe et la confirmation doivent être identiques";
        this.warning=true;
      }else if(this.user.email != this.form.value.email){
        this.message="L'adresse mail de correspond pas à celle du mail envoyé "
        this.warning=true;
      }else{
        let userRequest:UserRequest={
          password:this.form.value.pass
        }
        this.httpUser.updatePassword(userRequest,this.id).subscribe({
          next:(res)=>{
            this.message="Le mot de passe a bien été modifié";
            this.warning=false;
          },error:(err) =>{
            this.message="Erreur dans la modification du mot de passe";
            this.warning=true;
          },complete:()=>{
            setTimeout(()=>{
              this.message=""
            },3000)
          }
        })
      }
    }
  }
}
