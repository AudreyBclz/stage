import { Component, OnInit } from '@angular/core';
import { HttpUserService } from 'src/app/service/http-user.service';
import { NavService } from 'src/app/service/nav.service';
import { StorageService } from 'src/app/service/storage.service';

@Component({
  selector: 'app-mon-compte',
  templateUrl: './mon-compte.component.html',
  styleUrls: ['./mon-compte.component.css']
})
export class MonCompteComponent implements OnInit {

  user:any;
  id:number;
  constructor(private navService:NavService,private httpUserService:HttpUserService,private storageService:StorageService) { }

  ngOnInit(): void {
    this.navService.isItAdminPage(false);
    this.id=this.storageService.getUser().id;

    this.httpUserService.getUserById(this.id).subscribe({
      next:(res)=>{
        this.user=res;
      }
    })
  }
}
