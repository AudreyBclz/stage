import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbCarousel, NgbCarouselConfig, NgbCarouselModule, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';
import { HttpActuService } from 'src/app/service/http-actu.service';
import { NavService } from 'src/app/service/nav.service';

@Component({
  selector: 'app-actu',
  templateUrl: './actu.component.html',
  styleUrls: ['./actu.component.css'],
  providers: [NgbCarouselConfig]
})
export class ActuComponent implements OnInit {

  message:string="";
  actus:any;
  width:number;
  actusdiv3:number[]=[];
  actusdiv2:number[]=[];
  constructor(config:NgbCarouselConfig, private navService:NavService,private httpActu:HttpActuService) {
    config.interval = 7000;
		config.keyboard = false;
		config.pauseOnHover = false;
    config.wrap=true;
    this.width =window.innerWidth;
   }

  ngOnInit(): void {
    this.navService.isItAdminPage(false);
    this.httpActu.getDisplay().subscribe({
      next: (res)=>{
        this.actus=res;
        console.log(this.actus);
        for(let i=0;i<Math.floor(this.actus.length/3);i++){
          this.actusdiv3.push(i);
        }
        for(let i=0;i<Math.floor(this.actus.length/2);i++){
          this.actusdiv2.push(i);
        }
        console.log(this.actusdiv2);

      },error:(err)=>{
        this.message=err.message;
        console.log(this.message);

      }
    })
  }

}
