import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpUserService } from 'src/app/service/http-user.service';
import { NavService } from 'src/app/service/nav.service';

@Component({
  selector: 'app-liste-users',
  templateUrl: './liste-users.component.html',
  styleUrls: ['./liste-users.component.css']
})
export class ListeUsersComponent implements OnInit {

  message:String="";
  warning:boolean=false;
  closeResult='';
  idUser:number;
  users:any;
  constructor(private modalService:NgbModal,private navService: NavService,private httpUser:HttpUserService) { }

  ngOnInit(): void {
    this.navService.isItAdminPage(true);
    this.httpUser.getAll().subscribe({
      next:(res)=>{
        this.users=res;
      },
      error:(err)=>{
        this.message=err.message;
        this.warning=true;
      }
    })
  }

  open(content:any,id:number) {
    this.idUser=id;
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			},
		);
	}

  private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}
  supprUser(modal:NgbActiveModal){
    this.httpUser.delete(this.idUser).subscribe({
      next:(res)=>{
        this.message=res;
        this.warning=false;
      },error:(err)=>{
        this.message="Erreur dans la suppression. Réessayer";
        this.warning=true;
      },complete:()=>{
        this.httpUser.getAll().subscribe({
          next:(res)=>{
            this.users=res;
          }
        })
        setTimeout(()=>{
          this.message="";
        },3000)
      }
    })
    modal.close();
  }

}
