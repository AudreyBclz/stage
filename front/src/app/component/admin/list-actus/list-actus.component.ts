import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpActuService } from 'src/app/service/http-actu.service';
import { NavService } from 'src/app/service/nav.service';

@Component({
  selector: 'app-list-actus',
  templateUrl: './list-actus.component.html',
  styleUrls: ['./list-actus.component.css']
})
export class ListActusComponent implements OnInit {

  warning:boolean=false;
  message:string="";
  idActu:number;
  closeResult='';
  actus:any;

  constructor(private modalService:NgbModal,private navService:NavService,private httpActu:HttpActuService) { }

  ngOnInit(): void {
    this.navService.isItAdminPage(true);
    this.httpActu.getAll().subscribe({
      next:(res)=>{
        this.actus=res;
      }
    })
  }

  open(content:any,id:number) {
    this.idActu=id;
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			},
		);
	}

  private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}

  supprActu(modal:NgbActiveModal){
    this.httpActu.delete(this.idActu).subscribe({
      next:(res)=>{
        this.message="L'actualité a bien été supprimée";
        this.warning=false;
      },
      error:(err)=>{
        this.message = err.message;
        this.warning=true;
      },
      complete:()=>{
        // on met à jour la liste des actus
        this.httpActu.getAll().subscribe({
          next:(res)=> {this.actus=res
          console.log(this.actus);
          },
          error:(err)=>{
            this.message=err.message;
            console.log(this.message);

          }
        })
      }
    })
    modal.close();
  }
}
