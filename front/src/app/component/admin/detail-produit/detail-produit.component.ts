import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpProductService } from 'src/app/service/http-product.service';
import { NavService } from 'src/app/service/nav.service';

@Component({
  selector: 'app-detail-produit',
  templateUrl: './detail-produit.component.html',
  styleUrls: ['./detail-produit.component.css']
})
export class DetailProduitComponent implements OnInit {

  id:number;
  product:any;
  constructor(private navService:NavService,private route:ActivatedRoute,private httpProd:HttpProductService) { }

  ngOnInit(): void {
    this.id=this.route.snapshot.params['id'];
    this.httpProd.getOneById(this.id).subscribe({
      next:(res)=>{
        this.product=res
      }
    })
  }

}
