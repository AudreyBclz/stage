import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CategoryRequest } from 'src/app/model/CategoryRequest';
import { HttpCategoryService } from 'src/app/service/http-category.service';
import { NavService } from 'src/app/service/nav.service';

@Component({
  selector: 'app-modif-category',
  templateUrl: './modif-category.component.html',
  styleUrls: ['./modif-category.component.css']
})
export class ModifCategoryComponent implements OnInit {

  warning:boolean=false;
  message:String="";
  form:FormGroup;
  id:number;
  category:any;
  request:CategoryRequest;
  constructor(private navService:NavService, private fb:FormBuilder,private route: ActivatedRoute,private httpCat:HttpCategoryService) {
    this.form=fb.group({
      name:fb.control('',[Validators.required])
    })
   }
   get name(){
    return this.form.get('name');
   }

  ngOnInit(): void {
    this.navService.isItAdminPage(true);
    this.id=this.route.snapshot.params['id'];
    this.httpCat.getById(this.id).subscribe({
      next:(res)=>{
        this.category = res
      },
      error:(err)=> {
        this.message=err.message;
        this.warning=true;
      },
      complete:()=>{
        this.form.controls['name'].setValue(this.category.name);
      }
    })
  }
  modif(){
    if(this.form.valid){
      this.request={
        name:this.form.value.name
      }
      this.httpCat.update(this.request,this.id).subscribe({
        next:(res)=>{
          this.message="La catégorie a bien été modifiée";
          this.warning=false;
        },
        error:(err)=>{
          this.message="Erreur dans la modification de la catégorie. Réessayer";
          this.warning=true;
        },
        complete:()=>{
          setTimeout(()=>{
            this.message="";
          },3000)
        }
      })
    }
  }

}
