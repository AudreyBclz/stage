import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ModalDismissReasons, NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpCategoryService } from 'src/app/service/http-category.service';
import { NavService } from 'src/app/service/nav.service';

@Component({
  selector: 'app-list-categories',
  templateUrl: './list-categories.component.html',
  styleUrls: ['./list-categories.component.css']
})
export class ListCategoriesComponent implements OnInit {

  idCat:number;
  closeResult='';
  categories:any;
  message:String="";
  warning:boolean=false;
  constructor(private navService:NavService,private modalService:NgbModal,private httpCat:HttpCategoryService) {}


  ngOnInit(): void {
    this.navService.isItAdminPage(true);
    this.httpCat.getAll().subscribe({
      next:(res)=>{
        this.categories=res;
      }
    })
  }

  open(content:any,id:number) {
    this.idCat=id;
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			},
		);
	}

  private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}
  supprCat(modal:NgbActiveModal){
    this.httpCat.delete(this.idCat).subscribe({
      next:(res)=>{
        this.message=res;
        this.warning=false;
      },error:(err)=>{
        this.message = err.message;
        this.warning=true;
      },complete:()=>{
        setTimeout(()=>{
          this.message=""
        },3000)
        this.httpCat.getAll().subscribe({
          next:(res)=>this.categories=res
        })
      }
    })
    modal.close();
  }

}
