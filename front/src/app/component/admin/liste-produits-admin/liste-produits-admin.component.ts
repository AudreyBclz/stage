import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpProductService } from 'src/app/service/http-product.service';
import { NavService } from 'src/app/service/nav.service';


@Component({
  selector: 'app-liste-produits-admin',
  templateUrl: './liste-produits-admin.component.html',
  styleUrls: ['./liste-produits-admin.component.css']
})
export class ListeProduitsAdminComponent implements OnInit {

  message:String="";
  warning:boolean=false;
  closeResult='';
  products:any;
  collectionSize:number=0;
  constructor(private navService: NavService,private modalService:NgbModal,private httpProd:HttpProductService) { }

  ngOnInit(): void {
    this.navService.isItAdminPage(true);

    this.httpProd.getAll().subscribe({
      next:(res)=>{
        this.products=res;
        console.log(this.products);

        this.collectionSize = this.products.length;
        this.refreshProducts();
      },
      error:(err)=>{
        this.message=err.message;
        this.warning=true;
      }
    })

  }

  idProduct:number;
  page = 1;
	pageSize = 10;

	prods: any[];


	refreshProducts() {
		this.prods = this.products.map((prod: any, i: number) => ({ id: i + 1, ...prod })).slice(
			(this.page - 1) * this.pageSize,
			(this.page - 1) * this.pageSize + this.pageSize,
		);
	}

  open(content:any,id:number) {
    this.idProduct=id;
    console.log(this.idProduct);

		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			},
		);
	}

  private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}
  supprProduit(modal:NgbActiveModal){
    this.httpProd.delete(this.idProduct).subscribe({
      next:(res)=>{
        this.message=res;
        this.warning=false;
      },
      error:(err)=>{
        this.message=err.message;
        this.warning=true;
      },
      complete:()=>{
        setTimeout(()=>{
          this.message=""
        },3000)
        this.httpProd.getAll().subscribe({
          next:(res)=>{
            this.products=res;
            this.refreshProducts();
          }
        })
      }
    })
    modal.close();
  }

}
