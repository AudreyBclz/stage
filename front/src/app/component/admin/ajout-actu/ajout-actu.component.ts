import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ActuRequest } from 'src/app/model/ActuRequest';
import { HttpActuService } from 'src/app/service/http-actu.service';
import { NavService } from 'src/app/service/nav.service';
import { StorageService } from 'src/app/service/storage.service';

@Component({
  selector: 'app-ajout-actu',
  templateUrl: './ajout-actu.component.html',
  styleUrls: ['./ajout-actu.component.css']
})
export class AjoutActuComponent implements OnInit {

  warning:boolean=false;
  message:String="";
  form:FormGroup;
  id:number;
  request:ActuRequest;
  constructor(private fb:FormBuilder,private navService:NavService,private route:ActivatedRoute,private httpActu: HttpActuService,private storageService:StorageService) {
    this.form=this.fb.group({
      title:fb.control('',[Validators.required]),
      description:fb.control('',[Validators.required]),
      isDisplay:fb.control('',[Validators.required])
    })
   }
   get title(){
    return this.form.get('title');
   }
   get description(){
    return this.form.get('description')
   }
   get isDisplay(){
    return this.form.get('isDisplay');
   }

  ngOnInit(): void {
    this.navService.isItAdminPage(true);
    this.id=this.storageService.getUser().id;
  }
  ajoutActu(){
    if(this.form.valid){
      this.request={
        title:this.form.value.title,
        description:this.form.value.description,
        isDisplay:this.form.value.isDisplay,
        id_admin:this.id
      }
      this.httpActu.create(this.request).subscribe({
        next:(res)=>{
          this.message="L'actualité a bien été créée";
          this.warning=false;
        },
        error:(err)=>{
          this.message="Erreur dans la création de l'actualité";
          this.warning=true;
        },
        complete:()=>{
          setTimeout(()=>{
            this.message=""
          },3000)
        }
      })
    }
  }
  goBack(){
    window.history.back();
  }

}
