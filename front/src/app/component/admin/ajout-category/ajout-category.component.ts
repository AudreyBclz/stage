import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryRequest } from 'src/app/model/CategoryRequest';
import { HttpCategoryService } from 'src/app/service/http-category.service';
import { NavService } from 'src/app/service/nav.service';

@Component({
  selector: 'app-ajout-category',
  templateUrl: './ajout-category.component.html',
  styleUrls: ['./ajout-category.component.css']
})
export class AjoutCategoryComponent implements OnInit {

  message:String="";
  warning:boolean=false;
  categoryRequest:CategoryRequest;
  form:FormGroup
  constructor(private navService:NavService, private fb:FormBuilder,private httpCat:HttpCategoryService) {
    this.form=fb.group({
      name:fb.control('',[Validators.required])
    })
   }
   get name(){
    return this.form.get("name");
   }

  ngOnInit(): void {
    this.navService.isItAdminPage(true);
  }
  ajoutCat(){
    if(this.form.valid){
      this.categoryRequest={
        name:this.form.value.name
      }
      this.httpCat.create(this.categoryRequest).subscribe({
        next:(res)=>{
          this.message="La catégorie a bien été créée";
          this.warning=false;
        },
        error:(err)=>{
          this.message="Erreur dans la création. Réessayer";
          this.warning=true;
        },complete:()=>{
          setTimeout(()=>{
            this.message=""
          },3000)
        }
      })
    }
  }
  goBack(){
    window.history.back();
  }

}
