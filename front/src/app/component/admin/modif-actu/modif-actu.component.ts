import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ActuRequest } from 'src/app/model/ActuRequest';
import { ActuResponse } from 'src/app/model/ActuResponse';
import { HttpActuService } from 'src/app/service/http-actu.service';
import { NavService } from 'src/app/service/nav.service';
import { StorageService } from 'src/app/service/storage.service';

@Component({
  selector: 'app-modif-actu',
  templateUrl: './modif-actu.component.html',
  styleUrls: ['./modif-actu.component.css']
})
export class ModifActuComponent implements OnInit {

  actuRequest:ActuRequest;
  message:String="";
  warning:boolean=false;
  actu:any;
  form:FormGroup;
  id:number;
  idAdmin:number;
  constructor(private fb:FormBuilder,private navService:NavService,private route:ActivatedRoute,private httpActu:HttpActuService,private storageService:StorageService) {
  this.form=fb.group({
    title:fb.control('',[Validators.required]),
    description:fb.control('',[Validators.required]),
    isDisplay:fb.control('',[Validators.required])
  })
  }
  get title(){
    return this.form.get("title");
  }
  get description(){
    return this.form.get("description");
  }
  get isDisplay(){
    return this.form.get("isDisplay");
  }

  ngOnInit(): void {
    this.id=this.route.snapshot.params['id'];
    this.idAdmin=this.storageService.getUser().id;
    this.navService.isItAdminPage(true);
    this.httpActu.getById(this.id).subscribe({
      next:(res)=>{
        this.actu=res;
      },complete:()=>{
        this.form.controls["title"].setValue(this.actu.title);
        this.form.controls["description"].setValue(this.actu.description);
      }
    })
  }
  modif(){
    if(this.form.valid){
      this.actuRequest={
        title:this.form.value.title,
        description:this.form.value.description,
        isDisplay:this.form.value.isDisplay,
        id_admin:this.idAdmin
      }
      this.httpActu.updateActu(this.actuRequest,this.id).subscribe({
        next:(res)=>{
          this.message="L'actualité a bien été modifiée";
          this.warning=false;
        },
        error:(err)=>{
          this.message="Erreur dans la modification. Réessayer";
          this.warning=true;
        },
        complete:()=>setTimeout(()=>{
          this.message=""
        },3000)
      })
    }
  }

}
