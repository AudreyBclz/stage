import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductRequest } from 'src/app/model/ProductRequest';
import { HttpCategoryService } from 'src/app/service/http-category.service';
import { HttpProductService } from 'src/app/service/http-product.service';
import { NavService } from 'src/app/service/nav.service';

@Component({
  selector: 'app-ajout-produit',
  templateUrl: './ajout-produit.component.html',
  styleUrls: ['./ajout-produit.component.css']
})
export class AjoutProduitComponent implements OnInit {

  message:String="";
  warning:boolean=false;
  form:FormGroup;
  categories:any;
  categorie:any;
  product:ProductRequest;
  constructor(private navService:NavService, private fb:FormBuilder,private httpCat:HttpCategoryService,private httpProd:HttpProductService ) {
    this.form = this.fb.group({
      name:fb.control('',[Validators.required]),
      description:fb.control('',[Validators.required]),
      price:fb.control('',[Validators.required]),
      stock:fb.control('',[Validators.required]),
      category:fb.control('',[Validators.required])
    })
   }
   get name(){
    return this.form.get("name");
   }
   get description(){
    return this.form.get("description");
   }
   get price(){
    return this.form.get("price");
   }
   get stock(){
    return this.form.get("stock");
   }
   get category(){
    return this.form.get("category");
   }


  ngOnInit(): void {
    this.navService.isItAdminPage(true);
    this.httpCat.getAll().subscribe({
      next:(res)=>{
        this.categories=res
      }
    })
  }
  ajoutProduit(){
    if(this.form.valid){
      this.httpCat.getById(this.form.value.category).subscribe({
        next:(res)=>{
          this.categorie=res;
        },
        error:(err)=>{
          this.message=err.message;
          this.warning=true;
        },
        complete:()=>{
          this.product={
            name:this.form.value.name,
            description:this.form.value.description,
            price:this.form.value.price,
            stock:this.form.value.stock,
            sauce:null,
            category:this.categorie
          }
          this.httpProd.create(this.product).subscribe({
            next:(res)=>{
              this.message="Le produit a bien été ajouté."
              this.warning=false;
            },
            error:(err)=>{
              this.message=err.message;
              this.warning=true;
            },
            complete:()=>{
              setTimeout(()=>{
                this.message=""
              },3000)
            }
          })
        }
      })
    }
  }
  goBack(){
    window.history.back();
  }

}
