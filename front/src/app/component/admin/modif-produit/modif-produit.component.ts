import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CategoryResponse } from 'src/app/model/CategoryResponse';
import { ProductRequest } from 'src/app/model/ProductRequest';
import { HttpCategoryService } from 'src/app/service/http-category.service';
import { HttpProductService } from 'src/app/service/http-product.service';
import { NavService } from 'src/app/service/nav.service';

@Component({
  selector: 'app-modif-produit',
  templateUrl: './modif-produit.component.html',
  styleUrls: ['./modif-produit.component.css']
})
export class ModifProduitComponent implements OnInit {

  message:String="";
  warning:boolean=false;
  form:FormGroup;
  id:number;
  product:any;
  categories:any;
  catSelect:any;
  cat:CategoryResponse;
  productrequest:ProductRequest;
  constructor(private navService:NavService,private fb:FormBuilder,private route:ActivatedRoute,private httpProd:HttpProductService,private httpCat:HttpCategoryService) {
    this.form=fb.group({
      name:fb.control('',[Validators.required]),
      description:fb.control('',[Validators.required]),
      price:fb.control('',[Validators.required]),
      stock:fb.control('',[Validators.required]),
      category:fb.control('',[Validators.required])
    })
  }
  get name(){
    return this.form.value.name;
  }
  get description(){
    return this.form.value.description;
  }
  get price(){
    return this.form.value.price;
  }
  get stock(){
    return this.form.value.stock;
  }
  get category(){
    return this.form.value.category;
  }

  ngOnInit(): void {
  this.navService.isItAdminPage(true);
  this.id=this.route.snapshot.params['id'];
  this.httpCat.getAll().subscribe({
    next:(res)=>{
      this.categories=res;
    },
    error:(err)=>{
      this.message=err.message;
      this.warning=true;
      setTimeout(()=>{
        this.message=""
      },3000)
    }
  })
  this.httpProd.getOneById(this.id).subscribe({
    next:(res)=>{
      this.product=res;
    },
    error:(err)=>{
      this.message=err.message;
      this.warning=true;
      setTimeout(()=>{
        this.message=""
      },3000)
    },
    complete:()=>{
      this.form.controls['name'].setValue(this.product.name);
      this.form.controls['name'].markAsTouched
      this.form.controls['description'].setValue(this.product.description);
      this.form.controls['description'].markAsTouched;
      this.form.controls['price'].setValue(this.product.price);
      this.form.controls['price'].markAsTouched;
      this.form.controls['stock'].setValue(this.product.stock);
      this.form.controls['stock'].markAsTouched;
      this.form.controls['category'].setValue(this.product.category.id,{onlySelf:true})
      this.form.controls['category'].markAsTouched
    }
  })
  }
  modif(){
    if(this.form.valid){
      this.httpCat.getById(this.form.value.category).subscribe({
        next:(res)=>{
          this.catSelect=res;
          this.cat={
            id:this.catSelect.id,
            name:this.catSelect.name
          }
        },complete:()=>{
          this.productrequest={
            name:this.form.value.name,
            description:this.form.value.description,
            price:this.form.value.price,
            stock:this.form.value.stock,
            category:this.cat
          }
          this.httpProd.update(this.productrequest,this.product.id).subscribe({
            next:(res)=>{
              this.message="Le produit a bien été modifié";
              this.warning=false;
            },
            error:(err)=>{
              this.message="Erreur dans la modification du produit";
              this.warning=true;
            },
            complete:()=>{
              setTimeout(()=>{
                this.message=""
              },3000)
            }
          })
        }
      })
    }
  }

}
