import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderRequest } from 'src/app/model/OrderRequest';
import { HttpOrderService } from 'src/app/service/http-order.service';

@Component({
  selector: 'app-valide-commande',
  templateUrl: './valide-commande.component.html',
  styleUrls: ['./valide-commande.component.css']
})
export class ValideCommandeComponent implements OnInit {

  message:string="";
  warning:boolean=false;
  idOrder:number;
  order:any;
  constructor(private httpOrder: HttpOrderService,private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.idOrder = this.route.snapshot.params['idOrder'];
    this.httpOrder.findById(this.idOrder).subscribe({
      next:(res)=>{
        this.order=res
      },error:(err)=>{
        this.message=err.message;
        this.warning=true;
      },
      complete:()=>{
        let request:OrderRequest={
          status:"PAID"
        }
        this.httpOrder.updateStatus(request,this.idOrder).subscribe({
          next:(res)=>{
            this.message="La commande a bien été payée"
          },
          error:(err)=> {
            this.warning=true;
          },
        })
      }
    })

  }

}
