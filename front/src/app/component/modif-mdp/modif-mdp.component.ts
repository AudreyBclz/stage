import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpUserService } from 'src/app/service/http-user.service';
import { NavService } from 'src/app/service/nav.service';
import { SendInBlueService } from 'src/app/service/send-in-blue.service';

@Component({
  selector: 'app-modif-mdp',
  templateUrl: './modif-mdp.component.html',
  styleUrls: ['./modif-mdp.component.css']
})
export class ModifMdpComponent implements OnInit {

  message:string="";
  warning:boolean=false;
  form:FormGroup;
  user:any;
  constructor(private fb:FormBuilder,private navService: NavService,private httpSend: SendInBlueService,private httpUser:HttpUserService) {
    this.form=this.fb.group({
      email:this.fb.control('',[Validators.required,Validators.pattern("^([a-z._0-9]*)\@([a-z]{2,10})\\.[a-z]{2,4}$")])
    })
   }
   get email(){
    return this.form.get("email")
   }
  ngOnInit(): void {
    this.navService.isItAdminPage(false);
  }

  reset(){
    this.httpUser.getUserByEmail(this.form.value.email).subscribe({
      next:(res)=>{
        this.user=res;
        this.httpSend.send(this.form.value.email,this.user.id).subscribe({
          next:(res)=>{
            this.message="Le mail a bien été envoyé à l'adresse mail indiquée";
            this.warning=false;
          },error:(err) =>{
            this.message=err.message;
            this.warning=true;
          },
        })
      },error:()=>{
        this.message="Erreur dans la modification, l'utilisateur n'existe pas";
        this.warning=true;
      },complete:()=>{
        setTimeout(()=>{
          this.message=""
        },3000)
      }
    })
  }
}
