import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Cart } from 'src/app/model/Cart';
import { HttpProductService } from 'src/app/service/http-product.service';
import { NavService } from 'src/app/service/nav.service';
import { StorageService } from 'src/app/service/storage.service';

@Component({
  selector: 'app-liste-produits',
  templateUrl: './liste-produits.component.html',
  styleUrls: ['./liste-produits.component.css']
})
export class ListeProduitsComponent implements OnInit {

  products:any;
  prods:any[];
  collectionSize = 0;
  productCart:Cart;
  productSelect:any=null;
  sauces:any;
  sauceSelect:any;
  message:String="";
  warning:boolean=false;

  constructor(private navService: NavService,private modalService:NgbModal,private fb:FormBuilder,private httpProd:HttpProductService,private storageService:StorageService)
  {
    this.form=fb.group({
      sauce:fb.control(''),
      quantity:fb.control('',[Validators.required])
    })
	}
  get sauce(){
    return this.form.get('sauce')
  }
  get quantity(){
    return this.form.get('quantity')
  }

  ngOnInit(): void {
    this.navService.isItAdminPage(false);
    this.httpProd.getProductAvailable().subscribe({
      next:(res)=>{
        this.products=res;
        console.log(res);
        // Enlever le "produit" sans sauce que l'on ne peut acheter
        for(let i =0; i<this.products.length;i++){
          if(this.products[i].name=="Sans sauce"){
            this.products.splice(i,1)
          }
        }

        this.collectionSize=this.products.length;
        this.refreshProducts();
      }
    })
    this.httpProd.getProductByCat("sauce").subscribe({
      next:(res)=>{
        this.sauces=res
      }
    })


  }
  form:FormGroup;
  idProduct:number=0;
  closeResult='';
  idSauce:number=10;
  page = 1;
	pageSize = 10;




	refreshProducts() {
		this.prods = this.products.map((prod: any, i: number) => ({ id: i + 1, ...prod })).slice(
			(this.page - 1) * this.pageSize,
			(this.page - 1) * this.pageSize + this.pageSize,
		);
	}

  open(content:any,id:number) {
		this.idProduct=id;

    this.httpProd.getOneById(this.idProduct).subscribe({
      next:(res)=>{
        this.productSelect = res;
        console.log(this.productSelect);
      }})
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			},
		)
	}

  private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}

  ajouter(modal:NgbActiveModal){
    if(this.form.valid){
      let cart = this.storageService.getCart();
      console.log(this.idProduct);
      let count=0; // nombre de produit selectionné qui apparait dans le panier (ils peuvent apparaître plusieurs fois avec différentes sauces)

      this.httpProd.getOneById(this.idProduct).subscribe({
        next:(res)=>{
          this.productSelect = res;
          console.log(this.productSelect);

        },
        complete:()=>{
          if(this.form.value.sauce!=0){
            this.idSauce=this.form.value.sauce;
            console.log("!=0");

          }
          this.httpProd.getOneById(this.idSauce).subscribe({
            next:(res)=>{
              this.sauceSelect=res;
            },
            complete:()=>{
              this.productSelect.sauce=this.sauceSelect;
              this.productCart={
                product:this.productSelect,
                quantity:this.form.value.quantity
              }
              console.log(this.productCart);

              for(let i =0; i<cart.length;i++){
                if(cart[i].product.id==this.idProduct){
                  count+=cart[i].quantity;
                  console.log(count);

                }
              }
              if(count+this.productCart.quantity>this.productSelect.stock){
                this.message="Vous ne pouvez pas commander une quantité supérieure au stock";
                this.warning=true;
              }else{
                this.storageService.addProductToCart(this.productCart);
                this.message="Le produit a bien été ajouté au panier";
                this.warning=false;
              }
              setTimeout(()=>{
                this.message=""
              },3000)
            }
          })
        }
      })
    }
    modal.close();
  }
}
