import { Component, OnInit } from '@angular/core';
import { HttpOrderService } from 'src/app/service/http-order.service';
import { StorageService } from 'src/app/service/storage.service';

@Component({
  selector: 'app-histo-commande',
  templateUrl: './histo-commande.component.html',
  styleUrls: ['./histo-commande.component.css']
})
export class HistoCommandeComponent implements OnInit {

  message:string="";
  warning:boolean=false;
  idUser:number;
  orders:any;
  constructor(private storageService:StorageService,private httpOrder:HttpOrderService) { }

  ngOnInit(): void {
    this.idUser= this.storageService.getUser().id;
    this.httpOrder.findOrdersByUser(this.idUser).subscribe({
      next:(res)=>{
        this.orders=res
      },error:(err)=>{
        this.message="Vous n'avez pas encore commandé chez nous";
        this.warning=false;
      }
    })
  }
  getTotalOrder(order:any){
    let res = 0;
    for(let ol of order.orderlines){
      res+=(ol.quantity*ol.price)
    }
    return res;
  }

}
