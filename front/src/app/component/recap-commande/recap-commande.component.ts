import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderRequest } from 'src/app/model/OrderRequest';
import { HttpOrderService } from 'src/app/service/http-order.service';
import { HttpStripeService } from 'src/app/service/http-stripe.service';
import { NavService } from 'src/app/service/nav.service';
import { StorageService } from 'src/app/service/storage.service';

@Component({
  selector: 'app-recap-commande',
  templateUrl: './recap-commande.component.html',
  styleUrls: ['./recap-commande.component.css']
})
export class RecapCommandeComponent implements OnInit {

  message:String="";
  warning:boolean=false;
  total:number=0;
  prods:any;
  orderRequest:OrderRequest;
  orderResponse:any;
  constructor(private navService: NavService,private storageService:StorageService,private httpOrder:HttpOrderService,private httpStripe:HttpStripeService,private router:Router) { }

  ngOnInit(): void {
    this.navService.isItAdminPage(false);
    this.prods=this.storageService.getCart();
    if(this.prods.length==0){
      window.location.href="liste-produits";
    }else{
      this.total=this.storageService.getTotal();
    }
  }
  payer(){
    this.orderRequest={
      date:new Date(),
      user:this.storageService.getUser().id,
      productCarts:this.storageService.getCart()
    }
    this.httpOrder.create(this.orderRequest).subscribe({
      next:(res)=>{
        this.orderResponse=res;
        this.storageService.clearCart();
        console.log(this.orderResponse.id);

        this.httpStripe.payer(this.prods,this.storageService.getUser().id,this.orderResponse.id).subscribe({
          next:(res) =>{
            window.location.href=res
          }
        })
      },error:(err)=>{
        this.message="Erreur dans la création de la commande";
        this.warning=true;
      }
    })
  }

}
