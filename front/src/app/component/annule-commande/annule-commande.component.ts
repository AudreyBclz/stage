import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderRequest } from 'src/app/model/OrderRequest';
import { HttpOrderService } from 'src/app/service/http-order.service';

@Component({
  selector: 'app-annule-commande',
  templateUrl: './annule-commande.component.html',
  styleUrls: ['./annule-commande.component.css']
})
export class AnnuleCommandeComponent implements OnInit {

  message:string="";
  warning:boolean=false;
  idOrder:number;
  order:any;
  constructor(private route:ActivatedRoute,private httpOrder:HttpOrderService,private router:Router) { }

  ngOnInit(): void {
    console.log(document.referrer);

    this.idOrder = this.route.snapshot.params['idOrder'];
    this.httpOrder.findById(this.idOrder).subscribe({
      next:(res)=>{
        this.order=res
      },error:(err)=>{
        this.message=err.message;
        this.warning=true;
      },
      complete:()=>{
        let request:OrderRequest={
          status:"CANCELED"
        }
        this.httpOrder.updateStatus(request,this.idOrder).subscribe({
          next:(res)=>{
            this.message="La commande a bien été annulée"
          },
          error:(err)=> {
            this.warning=true;
          },
        })
      }
    })

  }

}
