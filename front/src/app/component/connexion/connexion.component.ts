import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserLoginRequest } from 'src/app/model/UserLoginRequest';
import { HttpUserService } from 'src/app/service/http-user.service';
import { NavService } from 'src/app/service/nav.service';
import { StorageService } from 'src/app/service/storage.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {
  message:string='';
  user:UserLoginRequest;
  response:any;
  form: FormGroup;
  constructor(private fb:FormBuilder, private navService: NavService,private httpService:HttpUserService,private route:Router,private storageService: StorageService) {
    this.form = this.fb.group({
      email:this.fb.control('',[Validators.required,Validators.pattern("^([a-z._0-9]*)\@([a-z]{2,10})\\.[a-z]{2,4}$")]),
      pass:this.fb.control('',[Validators.required,Validators.pattern("(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}")])
    });
   }
   get email(){
    return this.form.get("email")
  }
  get pass(){
    return this.form.get("pass")
  }
  ngOnInit(): void {
    this.navService.isItAdminPage(false);

  }
  submitCo(){
    if(this.form.valid){
      this.user = {
        username:this.form.value.email,
        password:this.form.value.pass
      }
      this.httpService.login(this.user).subscribe({
        next: (res) =>{
          this.response = res;
          this.storageService.saveAuth(this.response.token);
          this.storageService.saveUser(this.response);
          window.location.href='liste-produits'
        },
        error:(err)=>{
          this.message="Email ou mot de passe incorrect";
          setTimeout(()=>{
            this.message=""
          },3000)
        }
      })
    }
  }

}
