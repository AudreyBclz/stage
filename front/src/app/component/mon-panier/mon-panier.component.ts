import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NavService } from 'src/app/service/nav.service';
import { StorageService } from 'src/app/service/storage.service';

@Component({
  selector: 'app-mon-panier',
  templateUrl: './mon-panier.component.html',
  styleUrls: ['./mon-panier.component.css']
})
export class MonPanierComponent implements OnInit {

  message:String="";
  warning:boolean=false;
  idProduct:number;
  idSauce:number;
  closeResult='';
  prods:any;
  total:number=0;
  constructor(private navService:NavService,private modalService:NgbModal,private storageService:StorageService) { }

  ngOnInit(): void {
    this.navService.isItAdminPage(false);
    this.prods=this.storageService.getCart();
    this.total=this.storageService.getTotal();
  }

  open(content:any,idProduct:number,idSauce:number) {

    this.idProduct=idProduct;
    this.idSauce=idSauce;
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			},
		);
	}

  private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}
  supprProdToCart(modal:NgbActiveModal){
    let tab =this.storageService.supprProductToCart(this.idProduct,this.idSauce);
   this.total=this.storageService.getTotal();


    this.prods=tab[0];
    if(tab[1]){
      this.message="Le produit a bien été supprimé du panier";
      this.warning=false;
    }else{
      this.message="Erreur dans la suppression."
      this.warning=true;
    }
    modal.close();
  }

  addQuantity(idProduct:number,idSauce:number){
    this.prods=this.storageService.addQuantityToProdCart(idProduct,idSauce);
    this.total=this.storageService.getTotal();
  }

  removeQuantity(idProduct:number,idSauce:number){
    this.prods=this.storageService.removeQuantityToProdCart(idProduct,idSauce);
    this.total=this.storageService.getTotal();
  }
}
