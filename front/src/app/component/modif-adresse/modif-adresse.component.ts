import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { from } from 'rxjs';
import { AddressRequest } from 'src/app/model/AddressRequest';
import { UserRequest } from 'src/app/model/UserRequest';
import { HttpUserService } from 'src/app/service/http-user.service';
import { NavService } from 'src/app/service/nav.service';
import { StorageService } from 'src/app/service/storage.service';

@Component({
  selector: 'app-modif-adresse',
  templateUrl: './modif-adresse.component.html',
  styleUrls: ['./modif-adresse.component.css']
})
export class ModifAdresseComponent implements OnInit {

  form:FormGroup;
  id:number;
  user:any;
  userRequest:UserRequest;
  addressRequest:AddressRequest;
  message:string="";
  warning:boolean=false;

  constructor(private navService:NavService,private fb:FormBuilder,private httpUserService:HttpUserService,private storageService:StorageService) {
    this.form = this.fb.group({
      email:this.fb.control('',[,Validators.pattern("^([a-z.]*)\@{1}([a-z]{2,10})\\.{1}[a-z]{2,4}$")]),
      address: this.fb.control(''),
      zip: this.fb.control('',[Validators.pattern("^[0-9]{5}$")]),
      city: this.fb.control('')
    });
   }

   get email(){
    return this.form.get("email")
   }
   get address(){
    return this.form.get("address")
   }
   get zip(){
    return this.form.get("zip")
   }
   get city(){
    return this.form.get("city")
   }

  ngOnInit(): void {
    this.navService.isItAdminPage(false);
    this.id=this.storageService.getUser().id;
    this.httpUserService.getUserById(this.id).subscribe({
      next:(res)=>{
        this.user=res;
      },
      complete:() => {
        this.form.controls['email'].setValue(this.user.email);
        this.form.controls['address'].setValue(this.user.address.street);
        this.form.controls['zip'].setValue(this.user.address.zip);
        this.form.controls['city'].setValue(this.user.address.city)
      },
    })
  }

  modifAdresse(){
    if(this.form.valid){
      this.addressRequest={
        street:this.form.value.address,
        zip:this.form.value.zip,
        city:this.form.value.city
      }
      this.userRequest={
        email:this.form.value.email,
        address:this.addressRequest
      }
      this.httpUserService.updateUserAddressOrMail(this.userRequest,this.id).subscribe({
        next:(res)=>{
          this.message="Les coordonnées ont bien été mise à jour, vous devez vous reconnecter si vous avez changé de mail";
          this.warning=false;
          setTimeout(()=>{
            this.message="";
            if(this.storageService.getUser().username != this.userRequest.email){
              this.storageService.clearStorage();
              window.location.href="connexion";
            }
          },3000)

        },
        error:(err)=>{
          this.message="Erreur dans la mise à jour. Réessayer";
          setTimeout(()=>{
            this.message="";
          },3000)
          this.warning=true;
        }
      })
    }
  }
}
