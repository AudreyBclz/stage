import { Component, OnInit, SecurityContext } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeHtml, SafeScript } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AddressRequest } from 'src/app/model/AddressRequest';
import { UserRequest } from 'src/app/model/UserRequest';
import { HttpUserService } from 'src/app/service/http-user.service';
import { NavService } from 'src/app/service/nav.service';
import { StorageService } from 'src/app/service/storage.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {
  message:string="";
  adressRequest:AddressRequest;
  userRequest:UserRequest;
  form:FormGroup;
  constructor(private fb:FormBuilder,private navService:NavService,storageService:StorageService,private httpUserService:HttpUserService,private route:Router) {
    this.form = this.fb.group({
      nom: this.fb.control('',[Validators.required]),
      prenom: this.fb.control('',[Validators.required]),
      email:this.fb.control('',[Validators.required,Validators.pattern("^([a-z._0-9]*)\@{1}([a-z]{2,10})\\.{1}[a-z]{2,4}$")]),
      pass: this.fb.control('',[Validators.required,Validators.pattern("(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}")]),
      confPass: this.fb.control('',[Validators.required,Validators.pattern("(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}")]),
      address: this.fb.control('',[Validators.required]),
      zip: this.fb.control('',[Validators.required,Validators.pattern("^[0-9]{5}$")]),
      city: this.fb.control('',[Validators.required])
    });
   }

   get nom(){
    return this.form.get("nom")
   }
   get prenom(){
    return this.form.get("prenom")
   }
   get email(){
    return this.form.get("email")
   }
   get pass(){
    return this.form.get("pass")
   }
   get confPass(){
    return this.form.get("confPass")
   }
   get address(){
    return this.form.get("address")
   }
   get zip(){
    return this.form.get("zip")
   }
   get city(){
    return this.form.get("city")
   }

  ngOnInit(): void {
    this.navService.isItAdminPage(false);
   }

  submitIns(){
    if(this.form.valid){
      if(this.form.value.pass == this.form.value.confPass){
        this.adressRequest={
          street:this.form.value.address,
          zip:this.form.value.zip,
          city:this.form.value.city

        }
        this.userRequest = {
          lastName:this.form.value.nom,
          firstName:this.form.value.prenom,
          email:this.form.value.email,
          password:this.form.value.pass,
          address:this.adressRequest,
        }
        this.httpUserService.create(this.userRequest).subscribe({
          next:(res)=>{
            this.route.navigate(['connexion'])
          },
          error:(err)=> {
            console.log(err.message);
            this.message="Erreur dans l'inscription, l'adresse mail est peut-être déjà utilisée. Réessayer";

            setTimeout(()=>{
              this.message="";
            },3000)
          },
        })
      }else{
        this.message="Le mot de passe et la confirmation ne sont pas identiques";
        setTimeout(()=>{
          this.message="";
        },3000)
      }

    }
  }
}
