import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './component/accueil/accueil.component';
import { ActuComponent } from './component/actu/actu.component';
import { ConnexionComponent } from './component/connexion/connexion.component';
import { HoraireComponent } from './component/horaire/horaire.component';
import { InscriptionComponent } from './component/inscription/inscription.component';
import { MenuComponent } from './component/menu/menu.component';
import { ModifMdpComponent } from './component/modif-mdp/modif-mdp.component';
import { MonCompteComponent } from './component/mon-compte/mon-compte.component';
import { MonPanierComponent } from './component/mon-panier/mon-panier.component';
import { RecapCommandeComponent } from './component/recap-commande/recap-commande.component';
import { ListeProduitsComponent } from './component/liste-produits/liste-produits.component';
import { ListeProduitsAdminComponent } from './component/admin/liste-produits-admin/liste-produits-admin.component';
import { AjoutProduitComponent } from './component/admin/ajout-produit/ajout-produit.component';
import { AjoutActuComponent } from './component/admin/ajout-actu/ajout-actu.component';
import { AjoutCategoryComponent } from './component/admin/ajout-category/ajout-category.component';
import { ListCategoriesComponent } from './component/admin/list-categories/list-categories.component';
import { ListActusComponent } from './component/admin/list-actus/list-actus.component';
import { ModifCategoryComponent } from './component/admin/modif-category/modif-category.component';
import { ModifActuComponent } from './component/admin/modif-actu/modif-actu.component';
import { ModifProduitComponent } from './component/admin/modif-produit/modif-produit.component';
import { DetailProduitComponent } from './component/admin/detail-produit/detail-produit.component';
import { ListeUsersComponent } from './component/admin/liste-users/liste-users.component';
import { ModifAdresseComponent } from './component/modif-adresse/modif-adresse.component';
import { AuthGuardService } from './service/auth-guard.service';
import { AuthGuardAdminService } from './service/auth-guard-admin.service';
import { ValideCommandeComponent } from './component/valide-commande/valide-commande.component';
import { ErreurNotFoundComponent } from './component/erreur-not-found/erreur-not-found.component';
import { ModifMdpFormComponent } from './component/modif-mdp-form/modif-mdp-form.component';
import { AnnuleCommandeComponent } from './component/annule-commande/annule-commande.component';
import { HistoCommandeComponent } from './component/histo-commande/histo-commande.component';

const routes: Routes = [
  {path:"",component:AccueilComponent},
  {path:"inscription",component:InscriptionComponent},
  {path:"actus",component:ActuComponent},
  {path:"horaire",component: HoraireComponent},
  {path:"menu",component:MenuComponent},
  {path:"connexion",component:ConnexionComponent},
  {path:"mon-compte",component:MonCompteComponent,canActivate:[AuthGuardService]},
  {path:"modif-mot-de-passe",component:ModifMdpComponent},
  {path:"modif-mot-de-passe/:id",component:ModifMdpFormComponent},
  {path:"modif-adresse",component:ModifAdresseComponent,canActivate:[AuthGuardService]},
  {path:"mon-panier",component:MonPanierComponent,canActivate:[AuthGuardService]},
  {path:"recap-commande",component:RecapCommandeComponent,canActivate:[AuthGuardService]},
  {path:"historique-commande",component:HistoCommandeComponent,canActivate:[AuthGuardService]},
  {path:"confirmation-paiement/:idOrder",component:ValideCommandeComponent,canActivate:[AuthGuardService]},
  {path:"annulation-paiement/:idOrder",component:AnnuleCommandeComponent,canActivate:[AuthGuardService]},
  {path:"liste-produits",component:ListeProduitsComponent,canActivate:[AuthGuardService]},
  {path:"admin",component:ListeProduitsAdminComponent,canActivate:[AuthGuardAdminService]},
  {path:"admin/ajout-produit",component:AjoutProduitComponent,canActivate:[AuthGuardAdminService]},
  {path:"admin/ajout-actu",component:AjoutActuComponent,canActivate:[AuthGuardAdminService]},
  {path:"admin/ajout-categorie",component:AjoutCategoryComponent,canActivate:[AuthGuardAdminService]},
  {path:"admin/liste-categories",component:ListCategoriesComponent,canActivate:[AuthGuardAdminService]},
  {path:"admin/liste-actus",component:ListActusComponent,canActivate:[AuthGuardAdminService]},
  {path:"admin/liste-users",component:ListeUsersComponent,canActivate:[AuthGuardAdminService]},
  {path:"admin/modif-categorie/:id",component:ModifCategoryComponent,canActivate:[AuthGuardAdminService]},
  {path:"admin/modif-actu/:id",component:ModifActuComponent,canActivate:[AuthGuardAdminService]},
  {path:"admin/modif-produit/:id",component:ModifProduitComponent,canActivate:[AuthGuardAdminService]},
  {path:"admin/produit/:id",component:DetailProduitComponent,canActivate:[AuthGuardAdminService]},
  {path:"**",component:ErreurNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
