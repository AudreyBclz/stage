import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './component/shared/nav/nav.component';
import { NgbCarouselModule, NgbDropdownModule, NgbModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { AccueilComponent } from './component/accueil/accueil.component';
import { HoraireComponent } from './component/horaire/horaire.component';
import { MenuComponent } from './component/menu/menu.component';
import { ActuComponent } from './component/actu/actu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InscriptionComponent } from './component/inscription/inscription.component';
import { ConnexionComponent } from './component/connexion/connexion.component';
import { MonCompteComponent } from './component/mon-compte/mon-compte.component';
import { ModifMdpComponent } from './component/modif-mdp/modif-mdp.component';
import { MonPanierComponent } from './component/mon-panier/mon-panier.component';
import { RecapCommandeComponent } from './component/recap-commande/recap-commande.component';
import { ListeProduitsComponent } from './component/liste-produits/liste-produits.component';
import { ListeProduitsAdminComponent } from './component/admin/liste-produits-admin/liste-produits-admin.component';
import { AjoutProduitComponent } from './component/admin/ajout-produit/ajout-produit.component';
import { AjoutActuComponent } from './component/admin/ajout-actu/ajout-actu.component';
import { AjoutCategoryComponent } from './component/admin/ajout-category/ajout-category.component';
import { ListCategoriesComponent } from './component/admin/list-categories/list-categories.component';
import { ListActusComponent } from './component/admin/list-actus/list-actus.component';
import { ModifCategoryComponent } from './component/admin/modif-category/modif-category.component';
import { ModifActuComponent } from './component/admin/modif-actu/modif-actu.component';
import { ModifProduitComponent } from './component/admin/modif-produit/modif-produit.component';
import { DetailProduitComponent } from './component/admin/detail-produit/detail-produit.component';
import { ListeUsersComponent } from './component/admin/liste-users/liste-users.component';
import { ModifAdresseComponent } from './component/modif-adresse/modif-adresse.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { InterceptorInterceptor } from './interceptor/interceptor.interceptor';
import { DisplayPipe } from './pipe/display.pipe';
import { ValideCommandeComponent } from './component/valide-commande/valide-commande.component';
import { ErreurNotFoundComponent } from './component/erreur-not-found/erreur-not-found.component';
import { ModifMdpFormComponent } from './component/modif-mdp-form/modif-mdp-form.component';
import { AnnuleCommandeComponent } from './component/annule-commande/annule-commande.component';
import { HistoCommandeComponent } from './component/histo-commande/histo-commande.component';
import { StatusPipe } from './pipe/status.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    AccueilComponent,
    HoraireComponent,
    MenuComponent,
    ActuComponent,
    InscriptionComponent,
    ConnexionComponent,
    MonCompteComponent,
    ModifMdpComponent,
    MonPanierComponent,
    RecapCommandeComponent,
    ListeProduitsComponent,
    ListeProduitsAdminComponent,
    AjoutProduitComponent,
    AjoutActuComponent,
    AjoutCategoryComponent,
    ListCategoriesComponent,
    ListActusComponent,
    ModifCategoryComponent,
    ModifActuComponent,
    ModifProduitComponent,
    DetailProduitComponent,
    ListeUsersComponent,
    ModifAdresseComponent,
    DisplayPipe,
    ValideCommandeComponent,
    ErreurNotFoundComponent,
    ModifMdpFormComponent,
    AnnuleCommandeComponent,
    HistoCommandeComponent,
    StatusPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgbCarouselModule,
    HttpClientModule,
    NgbDropdownModule,
    NgbNavModule
  ],
  providers: [{
    provide:HTTP_INTERCEPTORS,
    useClass:InterceptorInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
