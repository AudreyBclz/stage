import { ProductResponse } from "./ProductResponse";

export interface Cart
{
  product:ProductResponse,
  quantity:number
}
