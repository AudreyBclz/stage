import { AddressRequest } from "./AddressRequest"

export interface UserRequest{
  lastName?: string
  firstName?: string
  email?: string
  password?: string
  address?: AddressRequest
}
