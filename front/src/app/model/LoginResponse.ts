export interface LoginResponse{
  token:string
  username:string
  password:string
  id:number
  role:string
}
