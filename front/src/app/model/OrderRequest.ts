import { Cart } from "./Cart";

export interface OrderRequest{
  date?:Date,
  user?:number,
  productCarts?:Cart[],
  status?:string
}
