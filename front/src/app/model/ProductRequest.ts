import { CategoryResponse } from "./CategoryResponse"

export interface ProductRequest{
  name:string
  description:string
  price:number
  stock:number
  sauce?:ProductRequest
  category:CategoryResponse
}
