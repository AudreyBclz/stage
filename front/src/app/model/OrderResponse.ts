export interface OrderResponse{
  id:number
  date:Date
  numOrder:string
  user:number
  isPaid:boolean
}
