import { CategoryResponse } from "./CategoryResponse"
import { ProductRequest } from "./ProductRequest"

export interface ProductResponse{
  name:string
  description:string
  price:number
  stock:number
  sauce?:ProductRequest
  category:CategoryResponse
}
