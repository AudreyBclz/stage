import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { tap } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class InterceptorInterceptor implements HttpInterceptor {

  user:any;
  constructor(private router: Router) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler) {
    let httpHeaders = new HttpHeaders();
    if(sessionStorage.getItem('userdetails')){
      this.user=JSON.parse(sessionStorage.getItem('userdetails'));
    }
    if(this.user &&this.user.password && this.user.token && this.user.username
      && this.user.role && this.user.id && sessionStorage.getItem('Authorization')==null){
      httpHeaders = httpHeaders.append(
        'Authorization','Basic '+window.btoa(this.user.username + ':' + this.user.password))
    }else{
      let authorization = sessionStorage.getItem('Authorization');
      if(authorization){
        httpHeaders = httpHeaders.append("Authorization",authorization)
      }
    }
    if(request.url=="https://api.sendinblue.com/v3/smtp/email"){
      httpHeaders= httpHeaders.append('accept','application/json')
      httpHeaders= httpHeaders.append('api-key','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');
      httpHeaders= httpHeaders.append('Content-Type','application/json')

    }
    httpHeaders = httpHeaders.append('X-Requested-With','XMLHttpRequest')

    const xhr = request.clone({
      headers:httpHeaders
    });
    return next.handle(xhr).pipe(tap(
      (error:any)=>{
        if(error instanceof HttpErrorResponse){
          if(error.status !==401){
            return;
          }
          this.router.navigate(['/accueil']);
        }
      }
      )
    )
  }
}
