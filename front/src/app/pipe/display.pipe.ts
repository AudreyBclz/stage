import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'display'
})
export class DisplayPipe implements PipeTransform {

  transform(value:boolean): string {
   if(value){
    return "Oui"
   }else{
    return "Non"
   }
  }

}
