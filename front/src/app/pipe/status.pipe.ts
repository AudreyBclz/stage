import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(value: string) {
    switch(value){
      case "PAID":
        return "Payée";
      case "INPROGRESS":
        return "En cours";
      case "CANCELED":
        return "Annulée"
    }
    return ""
  }

}
