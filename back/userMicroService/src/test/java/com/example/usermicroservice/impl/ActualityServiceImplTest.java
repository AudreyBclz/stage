package com.example.usermicroservice.impl;

import com.example.usermicroservice.dto.ActualityDto;
import com.example.usermicroservice.dto.ActualityRequestDto;
import com.example.usermicroservice.dto.ActualityResponseDto;
import com.example.usermicroservice.entity.Actuality;
import com.example.usermicroservice.entity.Address;
import com.example.usermicroservice.entity.User;
import com.example.usermicroservice.repository.ActualityRepository;
import com.example.usermicroservice.repository.UserRepository;
import com.example.usermicroservice.service.impl.ActualityServiceImpl;
import com.example.usermicroservice.tool.DtoUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;


@ExtendWith(MockitoExtension.class)
public class ActualityServiceImplTest {
    @Mock
    ActualityRepository actualityRepository;
    @Mock
    DtoUtils<Actuality,ActualityDto>dtoUtils;
    @Mock
    UserRepository userRepository;
    @InjectMocks
    private ActualityServiceImpl actualityService;
    private Actuality actu;
    private Actuality actu2;
    private ActualityRequestDto dto;
    private User user;
    private ActualityResponseDto responseDto;
    @BeforeEach
    void setUp(){
        dtoUtils=new DtoUtils<>();
       MockitoAnnotations.openMocks(this);
       dto=new ActualityRequestDto();
        dto.setDescription("Description");
        dto.setIsDisplay(true);
        dto.setId_admin(3L);

        user =new User();
        user.setFirstName("Maggie");
        user.setLastName("Jones");
        user.setEmail("maggie@email.com");
        user.setId_au(2L);
        user.setId(3L);
        user.setPassword("12334");
        user.setAddress(new Address());
        user.getAddress().setId(1L);
        user.getAddress().setStreet("48 rue des Fleurs");
        user.getAddress().setZip("75000");
        user.getAddress().setCity("Paris");

        actu= new Actuality();
        actu.setDescription("Description");
        actu.setIsDisplay(true);
        actu.setAdmin(user);
        actu2= new Actuality();
        actu2.setDescription("Description2");
        actu2.setIsDisplay(false);
        actu2.setAdmin(user);

        responseDto= new ActualityResponseDto();
        responseDto.setTitle("titre");
        responseDto.setDescription("description");
        responseDto.setIsDisplay(true);
        responseDto.setId_admin(3L);
        responseDto.setId(1L);
    }

    @Test
    void testCreateWillReturnExceptionIfAFieldMissing() {
        //Dans le beforeEach mon actu manque le champ titre
        Assertions.assertThrowsExactly(Exception.class,()->{
            actualityService.create(dto);
        });
    }
    @Test
    void testCreateWillReturnExceptionIfAdminNotExist(){
        dto.setTitle("titre");
        Optional<User> optionalUser = Optional.empty();
        // Ici j'ai utilisé lenient car le bouchonnage n'était pas nécessaire (unecessary stubbing quand je mettais le given)
        Mockito.lenient().when(userRepository.findById(3L)).thenReturn(optionalUser);
        Assertions.assertThrowsExactly(Exception.class,()->{
            actualityService.create(dto);
        });
    }
    @Test
    void testCreateWillReturnActualityInOtherCase() throws Exception {
        dto.setTitle("titre");

        ActualityResponseDto responseDto= new ActualityResponseDto();
        responseDto.setTitle("titre");
        responseDto.setDescription("description");
        responseDto.setIsDisplay(true);
        responseDto.setId_admin(3L);
        responseDto.setId(1L);

        Optional<User>optionalUser = Optional.of(user);
        given(userRepository.findById(3L)).willReturn(optionalUser);
        given(actualityRepository.save(actu)).willReturn(actu);
        given(dtoUtils.convertToEntity(new Actuality(),dto)).willReturn(actu);
        given(dtoUtils.convertToDto(actualityRepository.save(actu),new ActualityResponseDto())).willReturn(responseDto);
        Assertions.assertNotNull(actualityService.create(dto));
    }

    @Test
    void testUpdateWillReturnExceptionWhenAFieldIsMissing(){
        //Dans le beforeEach mon actu manque le champ titre
        Assertions.assertThrowsExactly(Exception.class,()->{
            actualityService.update(dto,1L);
        });
    }
    @Test
    void testUpdateWillReturnExceptionWhenActuNotExist(){
        dto.setTitle("titre");
        Optional<Actuality> optionalActuality = Optional.empty();
        Mockito.lenient().when(actualityRepository.findById(1L)).thenReturn(optionalActuality);
        Assertions.assertThrowsExactly(Exception.class,()->{
            actualityService.update(dto,1L);
        });
    }
    @Test
    void testUpdateWillReturnExceptionWhenAdminNotExist(){
        dto.setTitle("titre");
        Optional<User> optionalUser = Optional.empty();
        Optional<Actuality> optionalActuality = Optional.of(actu);
        Mockito.lenient().when(actualityRepository.findById(1L)).thenReturn(optionalActuality);
        Mockito.lenient().when(userRepository.findById(3L)).thenReturn(optionalUser);
        Assertions.assertThrowsExactly(Exception.class,()->{
            actualityService.update(dto,1L);
        });
    }
    @Test
    void testUpdateWillReturnActualityInOtherCase() throws Exception {
        dto.setTitle("titre");
        ActualityResponseDto responseDto= new ActualityResponseDto();
        responseDto.setTitle("titre");
        responseDto.setDescription("description");
        responseDto.setIsDisplay(true);
        responseDto.setId_admin(3L);
        responseDto.setId(1L);

        Optional<User>optionalUser = Optional.of(user);
        Optional<Actuality>optionalActuality = Optional.of(actu);
        given(userRepository.findById(3L)).willReturn(optionalUser);
        given(actualityRepository.findById(1L)).willReturn(optionalActuality);
        given(actualityRepository.save(actu)).willReturn(actu);
        given(dtoUtils.convertToDto(actualityRepository.save(actu),new ActualityResponseDto())).willReturn(responseDto);
        Assertions.assertNotNull(actualityService.update(dto,1L));
    }
    @Test
    void testDeleteWillReturnExceptionWhenActuNotExist(){
        Optional<Actuality> optionalActuality = Optional.empty();
        Mockito.lenient().when(actualityRepository.findById(1L)).thenReturn(optionalActuality);
        Assertions.assertThrowsExactly(Exception.class,()->{
            actualityService.delete(1L);
        });
    }
    @Test
    void testDeleteWillReturnStringWhenActuExist() throws Exception {
        given(actualityRepository.existsById(1L)).willReturn(true);
        Assertions.assertEquals(actualityService.delete(1L),"L'actualité a bien été supprimée");
    }
    @Test
    void testGetAllWillReturnExceptionWhenThereIsNoActivities(){
        given(actualityRepository.findAll()).willReturn(new ArrayList<>());
        Assertions.assertThrowsExactly(Exception.class,()->{
            actualityService.getAll();
        });
    }
    @Test
    void testGetAllWillReturnNotNullArrayOf2WhenThereIs2Activities()throws Exception{
        given(actualityRepository.findAll()).willReturn(Arrays.asList(actu,actu2));
        //Ici je lui dit que n'importe quel convertToDto retournera dans cette fonction responseDto
        given(dtoUtils.convertToDto(any(),any())).willReturn(responseDto);
        Assertions.assertTrue(actualityService.getAll().size()==2);
    }
    @Test
    void testGetDisplayActWillReturnExceptionWhenThereIsNoActivitiesToDisplay(){
        given((actualityRepository.findActualitiesByIsDisplayIsTrue())).willReturn(new ArrayList<>());
        Assertions.assertThrowsExactly(Exception.class,()->{
            actualityService.getDisplayAct();
        });

    }
    @Test
    void testGetDisplayActWillReturnNotNullArrayOf1WhenThereis1ActivityToDisplay() throws Exception {
        given(actualityRepository.findActualitiesByIsDisplayIsTrue()).willReturn(Arrays.asList(actu));
        given(dtoUtils.convertToDto(actu,new ActualityResponseDto())).willReturn(responseDto);
        Assertions.assertTrue(actualityService.getDisplayAct().size()==1);
    }
    @Test
    void testGetByIdWillReturnExceptionWhenActualityNotExist(){
        Optional<Actuality>optionalActuality = Optional.empty();
        given(actualityRepository.findById(1L)).willReturn(optionalActuality);
        Assertions.assertThrowsExactly(Exception.class,()->{
            actualityService.getById(1L);
        });
    }
    @Test
    void testGetByIdWillReturnActualityDtoWhenActualityExist() throws Exception {
        Optional<Actuality>optionalActuality = Optional.of(actu);
        given(actualityRepository.findById(1L)).willReturn(optionalActuality);
        given(dtoUtils.convertToDto(actu,new ActualityResponseDto())).willReturn(responseDto);
        Assertions.assertEquals(actualityService.getById(1L),responseDto);
    }
}

