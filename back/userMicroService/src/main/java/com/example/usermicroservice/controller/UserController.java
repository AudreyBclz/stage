package com.example.usermicroservice.controller;

import com.example.usermicroservice.dto.UserRequestDto;
import com.example.usermicroservice.dto.UserResponseDto;
import com.example.usermicroservice.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/user")
@CrossOrigin(value = "http://localhost:8083",methods = {RequestMethod.POST,RequestMethod.GET,RequestMethod.DELETE,RequestMethod.PUT,RequestMethod.PATCH})
public class UserController {

    @Autowired
    private UserServiceImpl _userService;
    @PostMapping("")
    ResponseEntity<UserResponseDto>create(@RequestBody UserRequestDto dto) throws Exception {
        return ResponseEntity.ok(_userService.create(dto));
    }
    @GetMapping("/{id}")
    ResponseEntity<UserResponseDto>findOneById(@PathVariable long id) throws Exception {
        return ResponseEntity.ok(_userService.findOne(id));
    }
    @GetMapping("search/{email}")
    ResponseEntity<UserResponseDto>findOneByEmail(@PathVariable String email) throws Exception {
        return ResponseEntity.ok(_userService.findOneByEmail(email));
    }
    @PutMapping("/{id}")
    ResponseEntity<UserResponseDto>updateAdressOrEmail(@RequestBody UserRequestDto dto,@PathVariable long id) throws Exception {
        return ResponseEntity.ok(_userService.updateUser(dto, id));
    }
    @GetMapping("")
    ResponseEntity<List<UserResponseDto>>findAll()throws Exception{
        return ResponseEntity.ok(_userService.findAll());
    }
    @DeleteMapping("/{id}")
    ResponseEntity<String>delete(@PathVariable long id)throws Exception{
        return ResponseEntity.ok(_userService.delete(id));
    }
    @PutMapping("/resetPassword/{id}")
    ResponseEntity<UserResponseDto>updatePassword(@PathVariable long id,@RequestBody UserRequestDto dto) throws Exception {
        return ResponseEntity.ok(_userService.updateMotDePasse(dto,id));
    }
}
