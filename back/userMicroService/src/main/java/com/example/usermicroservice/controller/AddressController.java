package com.example.usermicroservice.controller;

import com.example.usermicroservice.dto.AddressRequestDto;
import com.example.usermicroservice.dto.AddressResponseDto;
import com.example.usermicroservice.service.impl.AddressServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/address")
@CrossOrigin(value = "http://localhost:8083",methods = {RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class AddressController {

    @Autowired
    private AddressServiceImpl _addressService;

    @PostMapping("")
    public ResponseEntity<AddressResponseDto>create(@RequestBody AddressRequestDto dto) throws Exception {
        return ResponseEntity.ok(_addressService.create(dto));
    }
    @PutMapping("/{id}")
    public ResponseEntity<AddressResponseDto>edit(@RequestBody AddressRequestDto dto,@PathVariable long id) throws Exception {
        return ResponseEntity.ok(_addressService.update(dto,id));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<String>delete(@PathVariable long id){
        return ResponseEntity.ok(_addressService.delete(id));
    }
}
