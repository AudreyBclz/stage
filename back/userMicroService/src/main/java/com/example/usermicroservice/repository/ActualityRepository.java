package com.example.usermicroservice.repository;

import com.example.usermicroservice.entity.Actuality;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActualityRepository extends CrudRepository<Actuality,Long> {
    List<Actuality>findActualitiesByIsDisplayIsTrue();
}
