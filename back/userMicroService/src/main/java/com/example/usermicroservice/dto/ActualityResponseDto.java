package com.example.usermicroservice.dto;

import com.example.usermicroservice.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActualityResponseDto implements ActualityDto {
    private long id;
    private String title;
    private String description;
    private Boolean isDisplay;
    private long id_admin;
}
