package com.example.usermicroservice.service;

import com.example.usermicroservice.dto.UserRequestDto;
import com.example.usermicroservice.dto.UserResponseDto;

import java.util.List;

public interface UserService {

    UserResponseDto create(UserRequestDto dto) throws Exception;
    UserResponseDto findOne(long id) throws Exception;
    UserResponseDto findOneByEmail(String email) throws Exception;
    UserResponseDto updateUser(UserRequestDto dto,long id) throws Exception;
    List<UserResponseDto>findAll()throws Exception;
    String delete(long id)throws Exception;
    UserResponseDto updateMotDePasse(UserRequestDto dto,long id) throws Exception;
}
