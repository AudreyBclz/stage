package com.example.usermicroservice.service.impl;

import com.example.usermicroservice.dto.ActualityDto;
import com.example.usermicroservice.dto.ActualityRequestDto;
import com.example.usermicroservice.dto.ActualityResponseDto;
import com.example.usermicroservice.entity.Actuality;
import com.example.usermicroservice.repository.ActualityRepository;
import com.example.usermicroservice.repository.UserRepository;
import com.example.usermicroservice.service.ActualityService;
import com.example.usermicroservice.tool.DtoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ActualityServiceImpl implements ActualityService {
    @Autowired
    private ActualityRepository _actualityRepository;
    @Autowired
    private UserRepository _userRepository;
    @Autowired
    private DtoUtils<Actuality, ActualityDto> _dtoUtils;

    @Override
    public ActualityResponseDto create(ActualityRequestDto dto) throws Exception {
        if (dto.getTitle() != null && dto.getDescription() != null && dto.getId_admin() != 0 && dto.getIsDisplay() != null) {
            if (_userRepository.findById(dto.getId_admin()).isPresent()) {
                Actuality actuality = _dtoUtils.convertToEntity(new Actuality(), dto);
                actuality.setAdmin(_userRepository.findById(dto.getId_admin()).get());
                ActualityResponseDto dtoResponse = (ActualityResponseDto) _dtoUtils.convertToDto(_actualityRepository.save(actuality), new ActualityResponseDto());
                dtoResponse.setId_admin(actuality.getAdmin().getId());
                return dtoResponse;
            } else {
                throw new Exception("Il n'y a pas d'utilisateur avec cet identifiant");
            }

        } else {
            throw new Exception("Erreur dans la création de l'actualité");
        }
    }

    @Override
    public ActualityResponseDto update(ActualityRequestDto dto, long id) throws Exception {
        if (dto.getTitle() != null && dto.getDescription() != null && dto.getId_admin() != 0 && dto.getIsDisplay() != null) {
            Optional<Actuality> optActuality = _actualityRepository.findById(id);
            if (optActuality.isPresent()) {
                if (_userRepository.findById(dto.getId_admin()).isPresent()) {
                    optActuality.get().setAdmin(_userRepository.findById(dto.getId_admin()).get());
                    optActuality.get().setTitle(dto.getTitle());
                    optActuality.get().setDescription(dto.getDescription());
                    optActuality.get().setIsDisplay(dto.getIsDisplay());
                    ActualityResponseDto dtoResponse = (ActualityResponseDto) _dtoUtils.convertToDto(_actualityRepository.save(optActuality.get()), new ActualityResponseDto());
                    dtoResponse.setId_admin(optActuality.get().getAdmin().getId());
                    return dtoResponse;
                } else {
                    throw new Exception("Il n'y a pas d'utilisateur avec cet identifiant");
                }
            } else {
                throw new Exception("L'actualité n'existe pas");
            }
        } else {
            throw new Exception("Erreur dans la modification");
        }
    }

    @Override
    public String delete(long id) throws Exception {
        if (_actualityRepository.existsById(id)) {
            try {
                _actualityRepository.deleteById(id);
                return "L'actualité a bien été supprimée";
            } catch (Exception ex) {
               throw new Exception("Erreur dans la suppression");
            }
        } else {
            throw new Exception("L'actualité n'existe pas");
        }
    }

    @Override
    public List<ActualityResponseDto> getAll() throws Exception {
        List<Actuality> actualities = (List<Actuality>) _actualityRepository.findAll();
        List<ActualityResponseDto> dtos = new ArrayList<>();
        if (actualities.size() > 0) {
           return addToDto(actualities);
        } else {
            throw new Exception("Il n' y a pas d'actualités");
        }
    }

    @Override
    public List<ActualityResponseDto> getDisplayAct() throws Exception {
        List<Actuality>actualities = _actualityRepository.findActualitiesByIsDisplayIsTrue();
        List<ActualityResponseDto>dtos = new ArrayList<>();
        if(actualities.size()>0){
            return addToDto(actualities);
        }else {
            throw new Exception("Il n'y a pas d'actualités");
        }
    }

    @Override
    public ActualityResponseDto getById(long id) throws Exception {
        Optional<Actuality> optActu = _actualityRepository.findById(id);
        if(optActu.isPresent()){
            return (ActualityResponseDto) _dtoUtils.convertToDto(optActu.get(),new ActualityResponseDto());
        }else{
            throw new Exception("Il n'y a pas d'actualité à cet identifiant");
        }
    }

    private List<ActualityResponseDto>addToDto(List<Actuality> actualities){
        List<ActualityResponseDto>dtos = new ArrayList<>();
        for(Actuality a:actualities){
            ActualityResponseDto dto = (ActualityResponseDto) _dtoUtils.convertToDto(a,new ActualityResponseDto());
            if (_userRepository.findById(dto.getId_admin()).isPresent()) {
                dto.setId_admin(a.getAdmin().getId());
            }
            dtos.add(dto);
        }
        return dtos;
    }
}
