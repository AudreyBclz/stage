package com.example.usermicroservice.repository;

import com.example.usermicroservice.entity.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends CrudRepository<Address,Long> {

    Address findAddressByStreetContainingAndAndZipContainingAndCityContaining(String street,String zip,String city);
}
