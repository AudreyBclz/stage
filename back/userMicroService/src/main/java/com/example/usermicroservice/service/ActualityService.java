package com.example.usermicroservice.service;

import com.example.usermicroservice.dto.ActualityRequestDto;
import com.example.usermicroservice.dto.ActualityResponseDto;

import java.util.List;

public interface ActualityService {

    ActualityResponseDto create(ActualityRequestDto dto) throws Exception;
    ActualityResponseDto update(ActualityRequestDto dto, long id) throws Exception;
    String delete(long id) throws Exception;
    List<ActualityResponseDto> getAll() throws Exception;
    List<ActualityResponseDto>getDisplayAct()throws Exception;
    ActualityResponseDto getById(long id)throws Exception;
}