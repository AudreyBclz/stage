package com.example.usermicroservice.service;

import com.example.usermicroservice.dto.AddressRequestDto;
import com.example.usermicroservice.dto.AddressResponseDto;

public interface AddressService {
    AddressResponseDto create(AddressRequestDto requestDto) throws Exception;
    AddressResponseDto update(AddressRequestDto requestDto,long id) throws Exception;
    String delete(long id);
}
