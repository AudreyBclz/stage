package com.example.usermicroservice.controller;

import com.example.usermicroservice.dto.ActualityRequestDto;
import com.example.usermicroservice.dto.ActualityResponseDto;
import com.example.usermicroservice.service.impl.ActualityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/actuality")
@CrossOrigin(value = "http://localhost:8083",methods = {RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class ActualityController {
    @Autowired
    private ActualityServiceImpl _actualityService;
    @PostMapping("")
    ResponseEntity<ActualityResponseDto>create(@RequestBody ActualityRequestDto dto) throws Exception {
        return ResponseEntity.ok(_actualityService.create(dto));
    }
    @PutMapping("/{id}")
    ResponseEntity<ActualityResponseDto>update(@RequestBody ActualityRequestDto dto,@PathVariable long id)throws Exception{
        return ResponseEntity.ok(_actualityService.update(dto, id));
    }
    @DeleteMapping("/{id}")
    ResponseEntity<String>delete(@PathVariable long id)throws Exception{
        return ResponseEntity.ok(_actualityService.delete(id));
    }
    @GetMapping("")
    ResponseEntity<List<ActualityResponseDto>>getAll() throws Exception{
        return ResponseEntity.ok(_actualityService.getAll());
    }
    @GetMapping("/display")
    ResponseEntity<List<ActualityResponseDto>>getDisplay()throws Exception{
        return ResponseEntity.ok(_actualityService.getDisplayAct());
    }
    @GetMapping("/{id}")
    ResponseEntity<ActualityResponseDto>getById(@PathVariable long id)throws Exception{
        return ResponseEntity.ok(_actualityService.getById(id));
    }
}
