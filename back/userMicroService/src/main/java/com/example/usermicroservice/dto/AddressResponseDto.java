package com.example.usermicroservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressResponseDto implements AddressDto{
    private long id;
    private String street;
    private String zip;
    private String city;
}
