package com.example.usermicroservice.dto;

import com.example.usermicroservice.entity.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRequestDto implements UserDto {
    private String lastName;

    private String firstName;

    private String email;

    private String password;

    private Address address;
    private long id_au;
}
