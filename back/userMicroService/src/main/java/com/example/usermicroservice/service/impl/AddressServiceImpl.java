package com.example.usermicroservice.service.impl;

import com.example.usermicroservice.dto.AddressDto;
import com.example.usermicroservice.dto.AddressRequestDto;
import com.example.usermicroservice.dto.AddressResponseDto;
import com.example.usermicroservice.entity.Address;
import com.example.usermicroservice.repository.AddressRepository;
import com.example.usermicroservice.service.AddressService;
import com.example.usermicroservice.tool.DtoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressRepository _addressRepository;

    @Autowired
    private DtoUtils<Address, AddressDto>_dtoUtils;
    @Override
    public AddressResponseDto create(AddressRequestDto requestDto) throws Exception {
        if (requestDto.getStreet() != null && requestDto.getZip() != null && requestDto.getCity() != null) {
            Address address = _dtoUtils.convertToEntity(new Address(), requestDto);
            return (AddressResponseDto) _dtoUtils.convertToDto(_addressRepository.save(address), new AddressResponseDto());
        }else{
            throw new Exception("L'adresse, le code postal et la ville sont obligatoires");
        }
    }

    @Override
    public AddressResponseDto update(AddressRequestDto requestDto, long id) throws Exception {
        Optional<Address> addressAModif = _addressRepository.findById(id);
        if(addressAModif.isPresent()){
            if (requestDto.getStreet() != null && requestDto.getZip() != null && requestDto.getCity() != null) {
                Address address = _dtoUtils.convertToEntity(new Address(), requestDto);
                address.setId(addressAModif.get().getId());
                return (AddressResponseDto) _dtoUtils.convertToDto(_addressRepository.save(address), new AddressResponseDto());
            }else{
                throw new Exception("L'adresse, le code postal et la ville sont obligatoires");
            }
        }else{
            throw new Exception("L'identifiant de l'adresse que vous voulez modifier est incorrect");
        }
    }

    @Override
    public String delete(long id) {
        try{
            _addressRepository.deleteById(id);
            return "L'adresse a bien été supprimée";
        }catch (Exception e){
            return "Erreur: L'adresse n'a pas pu être supprimée, peut être que l'id n'existe pas";
        }
    }
}
