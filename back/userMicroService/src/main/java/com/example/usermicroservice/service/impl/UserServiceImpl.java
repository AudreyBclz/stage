package com.example.usermicroservice.service.impl;

import com.example.usermicroservice.dto.UserDto;
import com.example.usermicroservice.dto.UserRequestDto;
import com.example.usermicroservice.dto.UserResponseDto;
import com.example.usermicroservice.entity.Address;
import com.example.usermicroservice.entity.User;
import com.example.usermicroservice.repository.AddressRepository;
import com.example.usermicroservice.repository.UserRepository;
import com.example.usermicroservice.service.UserService;
import com.example.usermicroservice.tool.DtoUtils;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository _userRepository;
    @Autowired
    private AddressRepository _addressRepository;
    @Autowired
    private DtoUtils<User, UserDto> _dtoUtils;
    @Override
    public UserResponseDto create(UserRequestDto dto) throws Exception {
        if(dto.getFirstName()!=null
                && dto.getEmail()!=null
                && dto.getPassword()!=null
                && dto.getLastName()!=null
                && dto.getAddress().getStreet()!=null &&
                dto.getAddress().getZip()!=null &&
                dto.getAddress().getCity()!=null){
            if(_userRepository.findUserByEmail(dto.getEmail())!=null){
                throw new Exception("Adresse mail déjà utilisée.");
            }
            String hashPass = BCrypt.hashpw(dto.getPassword(),BCrypt.gensalt());
            dto.setPassword(hashPass);
            User user = _dtoUtils.convertToEntity(new User(),dto);
            user.setId_au(2L);
            //Je cherche si l'adresse existe en base de donnée si oui je la set à mon user, comme ça mon adresse est attaché et quand je save l'user elle ne sera pas créée en double
            Address addressFound=_addressRepository.findAddressByStreetContainingAndAndZipContainingAndCityContaining(dto.getAddress().getStreet(),dto.getAddress().getZip(),dto.getAddress().getCity());
            if(addressFound!=null){
                user.setAddress(addressFound);
            }else{
                _addressRepository.save(dto.getAddress());
            }
            return (UserResponseDto) _dtoUtils.convertToDto(_userRepository.save(user),new UserResponseDto());
        }else {
            throw new Exception("Le prénom, le nom, l'email, le mot de passe et l'adresse sont obligatoires");
        }
    }

    @Override
    public UserResponseDto findOne(long id) throws Exception {
        Optional<User> optionalUser = _userRepository.findById(id);
        if(optionalUser.isPresent()){
            return (UserResponseDto)_dtoUtils.convertToDto(optionalUser.get(),new UserResponseDto());
        }else {
            throw new Exception("Il n'y a pas d'utilisateur à cet identifiant");
        }
    }

    @Override
    public UserResponseDto findOneByEmail(String email) throws Exception {
        User user =_userRepository.findUserByEmail(email);
        if(user!=null){
            return (UserResponseDto) _dtoUtils.convertToDto(user,new UserResponseDto());
        }else{
            throw new Exception("Il n'y a pas d'utilisateur avec cet email");
        }
    }

    @Override
    public UserResponseDto updateUser(UserRequestDto dto, long id) throws Exception {
        Optional<User> user = _userRepository.findById(id);
        if(user.isPresent()){
            if(dto.getEmail()!=null){
                if(!dto.getEmail().equals(user.get().getEmail())){
                    user.get().setEmail(dto.getEmail());
                }
            }
            if(dto.getAddress()!=null){
                if(dto.getAddress().getStreet().length()==0 || dto.getAddress().getZip().length()==0 || dto.getAddress().getCity().length()==0){
                    throw new Exception("Si vous voulez changer l'adresse il faut remplir tout les champs");
                }
                if (!Objects.equals(dto.getAddress().getStreet(), user.get().getAddress().getStreet()) ||
                        !Objects.equals(dto.getAddress().getZip(), user.get().getAddress().getZip()) ||
                        !Objects.equals(dto.getAddress().getCity(), user.get().getAddress().getCity())
                ){
                    // Si l'adresse est modifiée, je vérifie si elle est en base de donnée si oui je la set si non je la créé.
                    Address addressModif = _addressRepository.findAddressByStreetContainingAndAndZipContainingAndCityContaining(dto.getAddress().getStreet(),dto.getAddress().getZip(),dto.getAddress().getCity());
                    if(addressModif!=null){
                        user.get().setAddress(addressModif);
                    }else{
                        Address newAddress =_addressRepository.save(dto.getAddress());
                        user.get().setAddress(newAddress);
                    }
                    return (UserResponseDto) _dtoUtils.convertToDto(_userRepository.save(user.get()),new UserResponseDto());
                }
            }

        }else{
            throw new Exception("Il n'y a pas d'utilisateur à cet identifiant");
        }
        return null;
    }

    @Override
    public List<UserResponseDto> findAll() throws Exception {
        List<User>users = (List<User>) _userRepository.findAll();
        List<UserResponseDto> userDtos = new ArrayList<>();
        if(users.size()>0){
            for(User u:users){
                UserResponseDto dto = (UserResponseDto) _dtoUtils.convertToDto(u,new UserResponseDto());
                userDtos.add(dto);
            }
            return userDtos;
        }else {
            throw new Exception("Il n'y pas d'utilisateur");
        }
    }

    @Override
    public String delete(long id) throws Exception {
       if(_userRepository.existsById(id)){
           try{
               _userRepository.deleteById(id);
               return "L'utilisateur a bien été supprimé";
           }catch (Exception e){
               throw new Exception("Erreur dans la suppression de l'utilisateur");
           }
       }else{
           throw new Exception("Une erreur est survenue. Veuillez réessayer.");
       }
    }

    @Override
    public UserResponseDto updateMotDePasse(UserRequestDto dto, long id) throws Exception {
        Optional<User> user = _userRepository.findById(id);
        if(user.isPresent()){
            user.get().setPassword(BCrypt.hashpw(dto.getPassword(),BCrypt.gensalt()));
            return (UserResponseDto) _dtoUtils.convertToDto(_userRepository.save(user.get()),new UserResponseDto());
        }else{
            throw new Exception("Il n'y a pas d'utilisateur à cet identifiant");
        }
    }
}
