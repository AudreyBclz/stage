package com.example.librairefriterie.enums;

public enum Status {
    INPROGRESS("inprogress"),
    CANCELED("canceled"),
    PAID("paid");

    private final String status;

    Status(String status){this.status=status;}
    public String getStatus(){return status;}
}
