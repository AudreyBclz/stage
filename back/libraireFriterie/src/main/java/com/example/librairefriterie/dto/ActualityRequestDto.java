package com.example.librairefriterie.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActualityRequestDto implements ActualityDto {
    private String title;
    private String description;
    private Boolean isDisplay;
    private long id_admin;
}
