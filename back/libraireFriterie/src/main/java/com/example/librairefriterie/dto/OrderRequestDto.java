package com.example.librairefriterie.dto;

import com.example.librairefriterie.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequestDto implements OrderDto{
    private Date date;
    private long user;
    private ProductCart[] productCarts;
    private Status status;
}
