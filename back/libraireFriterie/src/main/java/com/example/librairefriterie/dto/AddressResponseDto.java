package com.example.librairefriterie.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressResponseDto implements AddressDto {
    private long id;
    private String street;
    private String zip;
    private String city;
}
