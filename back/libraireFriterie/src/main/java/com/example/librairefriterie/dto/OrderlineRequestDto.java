package com.example.librairefriterie.dto;

import com.example.librairefriterie.model.Order;
import com.example.librairefriterie.model.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderlineRequestDto implements OrderlineDto{
    private int quantity;

    private String nameProduct;
    private double price;

    private Product product;

    private Order order;
}
