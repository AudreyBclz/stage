package com.example.librairefriterie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibraireFriterieApplication {

    public static void main(String[] args) {
        SpringApplication.run(LibraireFriterieApplication.class, args);
    }

}
