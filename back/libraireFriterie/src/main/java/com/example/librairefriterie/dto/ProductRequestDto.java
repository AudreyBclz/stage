package com.example.librairefriterie.dto;

import com.example.librairefriterie.model.Category;
import com.example.librairefriterie.model.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequestDto implements ProductDto {
    private String name;
    private String description;
    private double price;
    private int stock;
    private Product sauce;
    private Category category;
}
