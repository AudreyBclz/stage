package com.example.librairefriterie.dto;

import com.example.librairefriterie.model.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductCart {
    int quantity;
    Product product;

}
