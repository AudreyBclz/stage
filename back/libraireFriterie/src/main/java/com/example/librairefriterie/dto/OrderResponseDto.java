package com.example.librairefriterie.dto;

import com.example.librairefriterie.enums.Status;
import com.example.librairefriterie.model.Orderline;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@ToString
public class OrderResponseDto implements OrderDto{
    private long id;
    private Date date;
    private String numOrder;
    private long user;
    private Status status;
    private List<Orderline>orderlines;

    OrderResponseDto(long id,Date date,String numOrder,long user,Status status){
        this.id=id;
        this.date=date;
        this.numOrder=numOrder;
        this.user=user;
        this.status=status;
        this.orderlines=new ArrayList<>();
    }

}
