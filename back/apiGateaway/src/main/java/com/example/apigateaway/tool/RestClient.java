package com.example.apigateaway.tool;

import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class RestClient<T,V> {

    //"http://localhost:8081","http://localhost:8082",
    private List<String> servers = Arrays.asList("http://172.18.0.5:8081","http://172.18.0.4:8082");
    private RestTemplate template;
    private HttpHeaders headers;
    private HttpStatusCode status;

    public List<String>getServers() {return servers;}

    //Le HttpComponentsClientHttpRequestFactory sert à faire les méthodes Patch
    public RestClient(){
        template = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        headers=new HttpHeaders();
        headers.add("Accept","*/*");
        headers.add("content-type","application/json");
    }

    public T post(String server,String uri,V data,Class<T> type){
        HttpEntity<V>requestEntity = new HttpEntity<>(data,headers);
        ResponseEntity<T>responseEntity = template.exchange(server+"/"+uri, HttpMethod.POST,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
    public T get(String server,String uri,Class<T> type,long id){
        HttpEntity<String>requestEntity = new HttpEntity<>("",headers);
        ResponseEntity<T>responseEntity= template.exchange(server+"/"+uri+"/"+id,HttpMethod.GET,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
    public T getOrder(String server,String uri,Class<T> type,long id){
        HttpEntity<String>requestEntity = new HttpEntity<>("",headers);
        ResponseEntity<T>responseEntity= template.exchange(server+"/"+uri+"/"+id+"/order",HttpMethod.GET,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
    public T getAll(String server,String uri,Class<T> type){
        HttpEntity<String>requestEntity = new HttpEntity<>("",headers);
        ResponseEntity<T>responseEntity= template.exchange(server+"/"+uri,HttpMethod.GET,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
    public T getByString(String server,String uri, Class<T> type,String word){
        HttpEntity<String>requestEntity = new HttpEntity<>("",headers);
        ResponseEntity<T>responseEntity= template.exchange(server+"/"+uri+"/"+word,HttpMethod.GET,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
    public T update(String server,String uri,V data,Class<T>type,long id){
        HttpEntity<V>requestEntity = new HttpEntity<>(data,headers);
        ResponseEntity<T>responseEntity= template.exchange(server+"/"+uri+"/"+id,HttpMethod.PUT,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
    public T delete(String server,String uri,Class<T> type,long id){
        HttpEntity<String>requestEntity = new HttpEntity<>("",headers);
        ResponseEntity<T>responseEntity= template.exchange(server+"/"+uri+"/"+id,HttpMethod.DELETE,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
    public T patch(String server,String uri,V data,Class<T>type,long id){
        HttpEntity<V>requestEntity = new HttpEntity<>(data,headers);
        ResponseEntity<T>responseEntity = template.exchange(server+"/"+uri+"/"+id,HttpMethod.PATCH,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
    public T updatePassword(String server,String uri,V data,Class<T>type,long id){
        HttpEntity<V>requestEntity =new HttpEntity<>(data,headers);
        ResponseEntity<T>responseEntity = template.exchange(server+"/"+uri+"/"+id,HttpMethod.PUT,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }

}
