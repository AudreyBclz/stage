package com.example.apigateaway.service.Impl;

import com.example.apigateaway.entity.UserDetailImpl;
import com.example.apigateaway.model.User;
import com.example.apigateaway.service.UserService;
import com.example.apigateaway.tool.DtoUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private UserService _userService;
    @Autowired
    private DtoUser _dtoUtils;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = _dtoUtils.convertToEntity(_userService.getOneByEmail(username));
        return UserDetailImpl.build(user);
    }
}
