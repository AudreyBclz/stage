package com.example.apigateaway.service;
import com.example.apigateaway.dto.ActualityRequestDto;
import com.example.apigateaway.dto.ActualityResponseDto;
import com.example.apigateaway.tool.RestClient;
import org.springframework.stereotype.Service;

@Service
public class ActualityService {
    public ActualityResponseDto create (ActualityRequestDto dto){
        RestClient<ActualityResponseDto,ActualityRequestDto> restClient = new RestClient<>();
        return restClient.post(restClient.getServers().get(1),"api/actuality",dto,ActualityResponseDto.class);
    }
    public ActualityResponseDto update(ActualityRequestDto dto,long id){
        RestClient<ActualityResponseDto,ActualityRequestDto> restClient = new RestClient<>();
        return restClient.update(restClient.getServers().get(1),"api/actuality",dto,ActualityResponseDto.class,id);
    }
    public String delete(long id){
        RestClient<String,String> restClient = new RestClient<>();
        return restClient.delete(restClient.getServers().get(1),"api/actuality",String.class,id);
    }
    public ActualityResponseDto[] getAll(){
        RestClient<ActualityResponseDto[],String> restClient = new RestClient<>();
        return restClient.getAll(restClient.getServers().get(1),"api/actuality",ActualityResponseDto[].class);
    }
    public ActualityResponseDto[] getActDisplay(){
        RestClient<ActualityResponseDto[],String> restClient = new RestClient<>();
        return restClient.getAll(restClient.getServers().get(1),"api/actuality/display",ActualityResponseDto[].class);
    }
    public ActualityResponseDto getOneById(long id){
        RestClient<ActualityResponseDto,String> restClient = new RestClient<>();
        return restClient.get(restClient.getServers().get(1),"api/actuality",ActualityResponseDto.class,id);
    }
}
