package com.example.apigateaway.controller;

import com.example.apigateaway.service.AddressService;
import com.example.apigateaway.dto.AddressRequestDto;
import com.example.apigateaway.dto.AddressResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("address")
@CrossOrigin(value = "http://localhost:4200",methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@EnableMethodSecurity
public class AddressController {

    @Autowired
    private AddressService _addressService;
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PostMapping("")
    ResponseEntity<AddressResponseDto>create(@RequestBody AddressRequestDto dto){
        return ResponseEntity.ok(_addressService.create(dto));
    }
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PutMapping("/{id}")
    ResponseEntity<AddressResponseDto>update(@RequestBody AddressRequestDto dto,@PathVariable long id){
        return ResponseEntity.ok(_addressService.update(dto, id));
    }
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @DeleteMapping("/{id}")
    ResponseEntity<String>delete(@PathVariable long id){
        return ResponseEntity.ok(_addressService.delete(id));
    }

}
