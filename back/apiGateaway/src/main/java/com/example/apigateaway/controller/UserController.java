package com.example.apigateaway.controller;

import com.example.apigateaway.dto.UserRequestDto;
import com.example.apigateaway.dto.UserResponseDto;
import com.example.apigateaway.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("user")
@CrossOrigin(value = "http://localhost:4200",methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT,RequestMethod.PATCH})
@EnableMethodSecurity
public class UserController {

    @Autowired
    private UserService _userService;

    @PreAuthorize("permitAll()")
    @PostMapping("")
    public ResponseEntity<UserResponseDto>create(@RequestBody UserRequestDto dto){
        return ResponseEntity.ok(_userService.create(dto));
    }
    @PreAuthorize("permitAll()")
    @GetMapping("/{id}")
    public ResponseEntity<UserResponseDto>findOneById(@PathVariable long id){
        return ResponseEntity.ok(_userService.getOneById(id));
    }
    @PreAuthorize("permitAll()")
    @GetMapping("search/{email}")
    public ResponseEntity<UserResponseDto>findeOneByEmail(@PathVariable String email){
        return ResponseEntity.ok(_userService.getOneByEmail(email));
    }
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PutMapping("/{id}")
    public ResponseEntity<UserResponseDto>updateAddressOrEmail(@RequestBody UserRequestDto dto,@PathVariable long id){
        return ResponseEntity.ok(_userService.updateUser(dto, id));
    }
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("")
    public ResponseEntity<UserResponseDto[]>findAll(){
        return ResponseEntity.ok(_userService.findAll());
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<String>delete(@PathVariable long id){
        return ResponseEntity.ok(_userService.delete(id));
    }

    @PreAuthorize("permitAll()")
    @PutMapping("reset-password/{id}")
    public ResponseEntity<UserResponseDto>updatePassword(@RequestBody UserRequestDto dto,@PathVariable Long id){
        return ResponseEntity.ok(_userService.updatePassword(dto,id));
    }

}
