package com.example.apigateaway.service;

import com.example.apigateaway.dto.UserRequestDto;
import com.example.apigateaway.dto.UserResponseDto;
import com.example.apigateaway.tool.RestClient;

import org.springframework.stereotype.Service;

@Service
public class UserService {
    public UserResponseDto create(UserRequestDto dto){
        RestClient<UserResponseDto,UserRequestDto> restClient = new RestClient<>();
        return restClient.post(restClient.getServers().get(1),"api/user",dto,UserResponseDto.class);
    }
    public UserResponseDto getOneById(long id){
        RestClient<UserResponseDto,String> restClient = new RestClient<>();
        return restClient.get(restClient.getServers().get(1),"api/user",UserResponseDto.class,id);
    }
    public UserResponseDto getOneByEmail(String email){
        RestClient<UserResponseDto,String> restClient = new RestClient<>();
        return restClient.getByString(restClient.getServers().get(1),"api/user/search",UserResponseDto.class,email);
    }
    public UserResponseDto updateUser(UserRequestDto dto,long id){
        RestClient<UserResponseDto,UserRequestDto> restClient = new RestClient<>();
        return restClient.update(restClient.getServers().get(1),"api/user/",dto,UserResponseDto.class,id);
    }
    public UserResponseDto[]findAll(){
        RestClient<UserResponseDto[],String> restClient = new RestClient<>();
        return restClient.getAll(restClient.getServers().get(1),"api/user",UserResponseDto[].class);
    }
    public String delete(long id){
        RestClient<String,String> restClient = new RestClient<>();
        return restClient.delete(restClient.getServers().get(1),"api/user", String.class,id);
    }
    public UserResponseDto updatePassword(UserRequestDto dto,long id){
        RestClient<UserResponseDto,UserRequestDto> restClient = new RestClient<>();
        return restClient.updatePassword(restClient.getServers().get(1),"api/user/resetPassword",dto,UserResponseDto.class,id);
    }

}
