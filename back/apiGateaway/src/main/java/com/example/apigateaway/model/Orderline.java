package com.example.apigateaway.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Orderline {
    private long id;
    private int quantity;
    private String nameProduct;
    private double price;
    private Product product;
    private Order order;
}
