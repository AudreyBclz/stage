package com.example.apigateaway.service;

import com.example.apigateaway.dto.ProductRequestDto;
import com.example.apigateaway.dto.ProductResponseDto;
import com.example.apigateaway.tool.RestClient;

import org.springframework.stereotype.Service;

@Service
public class ProductService {
    public ProductResponseDto create(ProductRequestDto dto){
        RestClient<ProductResponseDto,ProductRequestDto> restClient= new RestClient<>();
        return restClient.post(restClient.getServers().get(0),"api/product",dto, ProductResponseDto.class);
    }
    public ProductResponseDto update(ProductRequestDto dto,long id){
        RestClient<ProductResponseDto,ProductRequestDto> restClient = new RestClient<>();
        return restClient.update(restClient.getServers().get(0),"api/product",dto, ProductResponseDto.class,id);
    }
    public ProductResponseDto[]getByCat(String category){
        RestClient<ProductResponseDto[],String> restClient = new RestClient<>();
        return restClient.getByString(restClient.getServers().get(0),"api/product/category", ProductResponseDto[].class,category);
    }
    public ProductResponseDto[]getAll(){
        RestClient<ProductResponseDto[],String> restClient = new RestClient<>();
        return restClient.getAll(restClient.getServers().get(0),"api/product",ProductResponseDto[].class);
    }
    public ProductResponseDto getOneById(long id){
        RestClient<ProductResponseDto,String> restClient = new RestClient<>();
        return restClient.get(restClient.getServers().get(0),"api/product", ProductResponseDto.class,id);
    }
    public String delete(long id){
        RestClient<String,String>restClient = new RestClient<>();
        return restClient.delete(restClient.getServers().get(0),"api/product", String.class,id);
    }
    public ProductResponseDto[]getProdAvailable(){
        RestClient<ProductResponseDto[],String>restClient = new RestClient<>();
        return restClient.getAll(restClient.getServers().get(0),"api/product/dispo",ProductResponseDto[].class);
    }
}
