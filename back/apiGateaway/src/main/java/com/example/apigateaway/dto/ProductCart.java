package com.example.apigateaway.dto;

import com.example.apigateaway.model.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductCart {
    int quantity;
    Product product;

}
