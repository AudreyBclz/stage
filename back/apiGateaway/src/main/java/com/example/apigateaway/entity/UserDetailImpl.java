package com.example.apigateaway.entity;

import com.example.apigateaway.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

public class UserDetailImpl implements UserDetails {
    private long id;
    private String username;
    private String password;

    private Collection<?extends GrantedAuthority> authorities;

    public UserDetailImpl(long id,String username,String password,Collection<?extends GrantedAuthority> authorities){
        this.id=id;
        this.username=username;
        this.password=password;
        this.authorities=authorities;
    }
    public static UserDetailImpl build(User user){
        Collection<SimpleGrantedAuthority> grantedAuthorities=new ArrayList<>();
              grantedAuthorities.add(new SimpleGrantedAuthority(user.getId_au().getName()));
              return new UserDetailImpl(user.getId(), user.getEmail(), user.getPassword(), grantedAuthorities);
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
