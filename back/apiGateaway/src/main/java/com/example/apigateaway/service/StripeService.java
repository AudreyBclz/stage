package com.example.apigateaway.service;

import com.example.apigateaway.dto.UserResponseDto;
import com.example.apigateaway.dto.ProductCart;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.LineItem;
import com.stripe.model.checkout.Session;
import com.stripe.param.checkout.SessionCreateParams;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;


@Service
public class StripeService {

    public String payerStripe(ProductCart[] productCarts, long idUser, long idOrder) throws StripeException {
        UserService userService = new UserService();
        UserResponseDto userResponseDto = userService.getOneById(idUser);

        Stripe.apiKey="sk_test_51JAEm7JqOqYYktTlzMFxc5qWGhQa0wknOI3zGzh0gxtLotGssvL1dlLZTFqdFFk4NKje8uI9C8sXKD4CRMCWkJOf00NcZAj9Kg";
            String YOUR_DOMAIN = "http://localhost:4200";
            List<LineItem> items = new ArrayList<>();

            SessionCreateParams.Builder params =
                    SessionCreateParams.builder()
                            .setCustomerEmail(userResponseDto.getEmail())
                            .setMode(SessionCreateParams.Mode.PAYMENT)
                            .setSuccessUrl(YOUR_DOMAIN + "/confirmation-paiement/"+idOrder)
                            .setCancelUrl(YOUR_DOMAIN + "/annulation-paiement/"+idOrder);
        for(ProductCart p:productCarts){
            SessionCreateParams.LineItem item= SessionCreateParams.LineItem.builder()
                    .setQuantity((long) p.getQuantity())
                    .setPriceData(SessionCreateParams.LineItem.PriceData.builder()
                            .setCurrency("eur")
                            .setUnitAmount((long) (p.getProduct().getPrice()*100))
                            .setProductData(SessionCreateParams.LineItem.PriceData.ProductData.builder()
                                    .setName(p.getProduct().getName())
                                    .setDescription(p.getProduct().getDescription())
                                    .build())
                            .build())
                    .build();
            params.addLineItem(item);
        }
            Session session = Session.create(params.build());
            return session.getUrl();

    }
}
