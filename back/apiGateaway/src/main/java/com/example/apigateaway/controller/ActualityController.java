package com.example.apigateaway.controller;

import com.example.apigateaway.dto.ActualityRequestDto;
import com.example.apigateaway.dto.ActualityResponseDto;
import com.example.apigateaway.service.ActualityService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("actuality")
@CrossOrigin(value = "http://localhost:4200",methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@EnableMethodSecurity
public class ActualityController {
    @Autowired
    private ActualityService _actualityService;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("")
    public ResponseEntity<ActualityResponseDto>create(@RequestBody ActualityRequestDto dto){
        return ResponseEntity.ok(_actualityService.create(dto));
    }
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<ActualityResponseDto>update(@RequestBody ActualityRequestDto dto, @PathVariable long id){
        return ResponseEntity.ok(_actualityService.update(dto,id));
    }
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<String>delete(@PathVariable long id){
        return ResponseEntity.ok(_actualityService.delete(id));
    }
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("")
    public ResponseEntity<ActualityResponseDto[]>getAll(){
        return ResponseEntity.ok(_actualityService.getAll());
    }
    @PreAuthorize("permitAll()")
    @GetMapping("/display")
    public ResponseEntity<ActualityResponseDto[]>getDisplayAct(){
        return ResponseEntity.ok(_actualityService.getActDisplay());
    }
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<ActualityResponseDto>getById(@PathVariable long id){
        return ResponseEntity.ok(_actualityService.getOneById(id));
    }
}
