package com.example.apigateaway.service;

import com.example.apigateaway.tool.RestClient;
import com.example.apigateaway.dto.AddressResponseDto;
import com.example.apigateaway.dto.AddressRequestDto;
import org.springframework.stereotype.Service;

@Service
public class AddressService {
    public AddressResponseDto create(AddressRequestDto dto){
        RestClient<AddressResponseDto,AddressRequestDto> restClient = new RestClient<>();
        return restClient.post(restClient.getServers().get(1),"api/address",dto, AddressResponseDto.class);
    }
    public AddressResponseDto update(AddressRequestDto dto,long id){
        RestClient<AddressResponseDto,AddressRequestDto> restClient= new RestClient<>();
        return restClient.update(restClient.getServers().get(1),"api/address",dto, AddressResponseDto.class,id);
    }

    public String delete(long id){
        RestClient<String,String>restClient=new RestClient<>();
        return restClient.delete(restClient.getServers().get(1),"api/address", String.class,id);
    }
}
