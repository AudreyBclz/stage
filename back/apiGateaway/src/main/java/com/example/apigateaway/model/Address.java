package com.example.apigateaway.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address {
    private long id;
    private String street;
    private String zip;
    private String city;
}
