package com.example.apigateaway.service;

import com.example.apigateaway.dto.OrderRequestDto;
import com.example.apigateaway.dto.OrderResponseDto;
import com.example.apigateaway.tool.RestClient;

import org.springframework.stereotype.Service;

@Service
public class OrderService {
    public OrderResponseDto create(OrderRequestDto dto) {
        RestClient<OrderResponseDto, OrderRequestDto> restClient = new RestClient<>();
        return restClient.post(restClient.getServers().get(0), "api/order", dto, OrderResponseDto.class);
    }
    public OrderResponseDto updateStatus(OrderRequestDto dto, long id){
        RestClient<OrderResponseDto,OrderRequestDto> restClient = new RestClient<>();
        return restClient.patch(restClient.getServers().get(0),"api/order/status",dto, OrderResponseDto.class,id);
    }
    public OrderResponseDto[]getOrderByUser(long idUser){
        RestClient<OrderResponseDto[],String> restClient = new RestClient<>();
        return restClient.get(restClient.getServers().get(0),"api/order/user", OrderResponseDto[].class,idUser);
    }
    public OrderResponseDto getOrderById(long id){
        RestClient<OrderResponseDto,String> restClient= new RestClient<>();
        return restClient.get(restClient.getServers().get(0),"api/order", OrderResponseDto.class,id);
    }
}
