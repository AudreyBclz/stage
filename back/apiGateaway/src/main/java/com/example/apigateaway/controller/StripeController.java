package com.example.apigateaway.controller;

import com.example.apigateaway.dto.ProductCart;
import com.example.apigateaway.service.StripeService;

import com.stripe.exception.StripeException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("paiement")
@CrossOrigin(value = "http://localhost:4200",methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE, RequestMethod.PUT})
@EnableMethodSecurity
public class StripeController {

    @PostMapping("/{id}/order/{idOrder}")
    public  ResponseEntity<String> create(@RequestBody ProductCart[] productCarts, @PathVariable long id, @PathVariable long idOrder) throws StripeException {
        StripeService service = new StripeService();
        return ResponseEntity.ok(service.payerStripe(productCarts,id,idOrder));
    }
}

