package com.example.apigateaway.dto;

import com.example.apigateaway.model.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestDto implements UserDto {
    private String lastName;

    private String firstName;

    private String email;

    private String password;

    private Address address;
    private long id_au;
}
