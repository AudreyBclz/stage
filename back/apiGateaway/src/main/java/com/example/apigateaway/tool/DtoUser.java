package com.example.apigateaway.tool;


import com.example.apigateaway.dto.UserResponseDto;
import com.example.apigateaway.entity.Authority;
import com.example.apigateaway.model.User;
import org.springframework.stereotype.Service;

@Service
public class DtoUser {
    public User convertToEntity(UserResponseDto dto){
        User user = new User();
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());
        user.setAddress(dto.getAddress());
        user.setPassword(dto.getPassword());
        user.setId(dto.getId());
        Authority authority = new Authority();
        if(dto.getId_au()==1){
            authority.setId(1L);
            authority.setName("ROLE_ADMIN");
        } else if (dto.getId_au()==2) {
            authority.setId(2L);
            authority.setName("ROLE_USER");
        }
        user.setId_au(authority);
        return user;
    }

    public UserResponseDto convertToDto(User user){
        return new UserResponseDto(user.getId(), user.getLastName(), user.getFirstName(), user.getEmail(), user.getPassword(), user.getAddress(),user.getId_au().getId());
    }
}
