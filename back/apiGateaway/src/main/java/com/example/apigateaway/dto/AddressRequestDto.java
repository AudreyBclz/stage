package com.example.apigateaway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressRequestDto implements AddressDto {
    private String street;
    private String zip;
    private String city;
}
