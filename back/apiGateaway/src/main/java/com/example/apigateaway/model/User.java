package com.example.apigateaway.model;

import com.example.apigateaway.entity.Authority;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private long id;
    private String lastName;
    private String firstName;
    private String email;
    private String password;
    private Address address;
    private Authority id_au;
}
