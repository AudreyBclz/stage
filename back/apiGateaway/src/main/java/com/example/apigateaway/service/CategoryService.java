package com.example.apigateaway.service;

import com.example.apigateaway.dto.CategoryRequestDto;
import com.example.apigateaway.dto.CategoryResponseDto;
import com.example.apigateaway.tool.RestClient;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {
    public CategoryResponseDto create(CategoryRequestDto dto){
        RestClient<CategoryResponseDto, CategoryRequestDto> restClient = new RestClient<>();
        return restClient.post(restClient.getServers().get(0),"api/category",dto, CategoryResponseDto.class);
    }
    public CategoryResponseDto update(CategoryRequestDto dto,long id){
        RestClient<CategoryResponseDto,CategoryRequestDto> restClient = new RestClient<>();
        return restClient.update(restClient.getServers().get(0),"api/category",dto, CategoryResponseDto.class,id);
    }
    public String delete(long id){
        RestClient<String,String> restClient = new RestClient<>();
        return restClient.delete(restClient.getServers().get(0),"api/category",String.class,id);
    }
    public CategoryResponseDto getById(long id){
        RestClient<CategoryResponseDto,String> restClient = new RestClient<>();
        return restClient.get(restClient.getServers().get(0),"api/category", CategoryResponseDto.class,id);
    }
    public CategoryResponseDto[] findAll(){
        RestClient<CategoryResponseDto[],String> restClient = new RestClient<>();
        return restClient.getAll(restClient.getServers().get(0),"api/category", CategoryResponseDto[].class);
    }
}
