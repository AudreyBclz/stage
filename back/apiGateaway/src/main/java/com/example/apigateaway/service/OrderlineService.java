package com.example.apigateaway.service;

import com.example.apigateaway.dto.OrderlineRequestDto;
import com.example.apigateaway.dto.OrderlineResponseDto;
import com.example.apigateaway.tool.RestClient;

import org.springframework.stereotype.Service;

@Service
public class OrderlineService {
    public OrderlineResponseDto create(OrderlineRequestDto dto){
        RestClient<OrderlineResponseDto,OrderlineRequestDto>restClient= new RestClient<>();
        return restClient.post(restClient.getServers().get(0),"api/orderline",dto,OrderlineResponseDto.class);
    }
    public OrderlineResponseDto[]findByOrder(long idOrder){
        RestClient<OrderlineResponseDto[],String>restClient= new RestClient<>();
        return restClient.getOrder(restClient.getServers().get(0),"api/orderline",OrderlineResponseDto[].class,idOrder);
    }
}
