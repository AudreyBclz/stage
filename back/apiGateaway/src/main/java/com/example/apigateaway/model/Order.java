package com.example.apigateaway.model;

import com.example.apigateaway.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private long id;
    private Date date;
    private long id_u;
    private Boolean isPaid;
    private Status status;
}
