package com.example.apigateaway.controller;

import com.example.apigateaway.dto.ProductRequestDto;
import com.example.apigateaway.dto.ProductResponseDto;
import com.example.apigateaway.service.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("product")
@CrossOrigin(value = "http://localhost:4200",methods =
        {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE, RequestMethod.PUT})
@EnableMethodSecurity
public class ProductController {
    @Autowired
    private ProductService _productService;
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("")
    public ResponseEntity<ProductResponseDto>create(@RequestBody ProductRequestDto dto){
        return ResponseEntity.ok(_productService.create(dto));
    }
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<ProductResponseDto>update(@RequestBody ProductRequestDto dto,@PathVariable long id){
        return ResponseEntity.ok(_productService.update(dto, id));
    }
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/category/{category}")
    public ResponseEntity<ProductResponseDto[]>getByCat(@PathVariable String category){
        return ResponseEntity.ok(_productService.getByCat(category));
    }
    @PreAuthorize("permitAll()")
    @GetMapping("")
    public ResponseEntity<ProductResponseDto[]>getAll(){
        return ResponseEntity.ok(_productService.getAll());
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/{id}")
    public ResponseEntity<ProductResponseDto>getById(@PathVariable long id){return ResponseEntity.ok(_productService.getOneById(id));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<String>delete(@PathVariable long id){
        return ResponseEntity.ok(_productService.delete(id));
    }

    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    @GetMapping("/dispo")
    public ResponseEntity<ProductResponseDto[]>getAllAvailable(){
        return ResponseEntity.ok(_productService.getProdAvailable());
    }
}
