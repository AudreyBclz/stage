package com.example.apigateaway.controller;

import com.example.apigateaway.dto.OrderRequestDto;
import com.example.apigateaway.dto.OrderResponseDto;
import com.example.apigateaway.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("order")
@CrossOrigin(value = "http://localhost:4200",methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE, RequestMethod.PUT,RequestMethod.PATCH})
@EnableMethodSecurity
public class OrderController {
    @Autowired
    private OrderService _orderService;

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PostMapping("")
    public ResponseEntity<OrderResponseDto>create(@RequestBody OrderRequestDto dto){
        return ResponseEntity.ok(_orderService.create(dto));
    }
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PatchMapping("/status/{id}")
    public ResponseEntity<OrderResponseDto>paid(@RequestBody OrderRequestDto dto, @PathVariable long id){
        return ResponseEntity.ok(_orderService.updateStatus(dto,id));
    }
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/user/{idUser}")
    public ResponseEntity<OrderResponseDto[]>getOrderByUser(@PathVariable long idUser){
        return ResponseEntity.ok(_orderService.getOrderByUser(idUser));
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/{id}")
    public ResponseEntity<OrderResponseDto>findOrderById(@PathVariable long id){
        return ResponseEntity.ok(_orderService.getOrderById(id));
    }
}
