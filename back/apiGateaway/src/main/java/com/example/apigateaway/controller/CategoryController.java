package com.example.apigateaway.controller;

import com.example.apigateaway.dto.CategoryRequestDto;
import com.example.apigateaway.dto.CategoryResponseDto;
import com.example.apigateaway.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("category")
@CrossOrigin(value = "http://localhost:4200",methods = {RequestMethod.GET,RequestMethod.POST,
        RequestMethod.DELETE,RequestMethod.PUT})
@EnableMethodSecurity
public class CategoryController {
    @Autowired
    private CategoryService _categoryService;
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("")
    ResponseEntity<CategoryResponseDto>create(@RequestBody CategoryRequestDto dto){
        return ResponseEntity.ok(_categoryService.create(dto));
    }
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    ResponseEntity<CategoryResponseDto>update(@RequestBody CategoryRequestDto dto,@PathVariable long id){
        return ResponseEntity.ok(_categoryService.update(dto, id));
    }
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    ResponseEntity<String>delete(@PathVariable long id){
        return ResponseEntity.ok(_categoryService.delete(id));
    }
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/{id}")
    ResponseEntity<CategoryResponseDto>findById(@PathVariable long id){
        return ResponseEntity.ok(_categoryService.getById(id));
    }
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("")
    ResponseEntity<CategoryResponseDto[]>findAll(){
        return ResponseEntity.ok(_categoryService.findAll());
    }
}
