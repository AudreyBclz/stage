package com.example.apigateaway.controller;

import com.example.apigateaway.dto.LoginRequestDto;
import com.example.apigateaway.dto.LoginResponseDto;
import com.example.apigateaway.model.User;
import com.example.apigateaway.service.UserService;
import com.example.apigateaway.tool.DtoUser;
import com.example.apigateaway.tool.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("")
@CrossOrigin(value = {"http://localhost:4200"},methods =
        {RequestMethod.GET,RequestMethod.DELETE,RequestMethod.POST})
@EnableMethodSecurity
public class LoginController {
    @Autowired
    private AuthenticationManager _authenticationManager;
    @Autowired
    private JwtUtils _jwtUtils;
    @Autowired
    private DtoUser _dtoUtils;
    @Autowired
    private UserService _userService;


    @PreAuthorize("permitAll()")
    @PostMapping("/login")
    public ResponseEntity<LoginResponseDto>loginUser(@RequestBody LoginRequestDto dto) throws Exception{
        try{
            Authentication authentication = _authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(dto.getUsername(),dto.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = _jwtUtils.generateJwtToken(authentication);
            User user = _dtoUtils.convertToEntity(_userService.getOneByEmail(dto.getUsername()));
            return ResponseEntity.ok(LoginResponseDto.builder().token(token).username(
                    dto.getUsername()).id(user.getId()).role(user.getId_au().getName()).build());
        }catch (Exception ex){
            throw ex;
        }
    }
}
