package com.example.apigateaway.controller;

import com.example.apigateaway.dto.OrderlineRequestDto;
import com.example.apigateaway.dto.OrderlineResponseDto;
import com.example.apigateaway.service.OrderlineService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("orderline")
@CrossOrigin(value = "http://localhost:4200",methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE, RequestMethod.PUT,RequestMethod.PATCH})
@EnableMethodSecurity
public class OrderlineController {
    @Autowired
    private OrderlineService _orderlineService;

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PostMapping("")
    public ResponseEntity<OrderlineResponseDto>create(@RequestBody OrderlineRequestDto dto){
        return ResponseEntity.ok(_orderlineService.create(dto));
    }
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/{idOrder}/order")
    public ResponseEntity<OrderlineResponseDto[]>getByOrder(@PathVariable long idOrder){
        return ResponseEntity.ok(_orderlineService.findByOrder(idOrder));
    }

}
