package com.example.productordermicroservice.entity;

import com.example.productordermicroservice.enums.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "num_order")
    private String numOrder;
    private Date date;
    @Column(name = "id_u")
    private long user;
    private Status status;
    @OneToMany(mappedBy = "order")
    @JsonIgnore
    List<Orderline>orderlines=new ArrayList<>();
}
