package com.example.productordermicroservice.service.impl;

import com.example.productordermicroservice.dto.CategoryDto;
import com.example.productordermicroservice.dto.CategoryRequestDto;
import com.example.productordermicroservice.dto.CategoryResponseDto;
import com.example.productordermicroservice.entity.Category;
import com.example.productordermicroservice.repository.CategoryRepository;
import com.example.productordermicroservice.service.CategoryService;
import com.example.productordermicroservice.tool.DtoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryRepository _categoryRepository;
    @Autowired
    private DtoUtils<Category, CategoryDto>_dtoUtils;
    @Override
    public CategoryResponseDto findCatByName(String name) throws Exception {
      Category category = _categoryRepository.findCategoryByName(name);
        if(category!=null){
            return (CategoryResponseDto) _dtoUtils.convertToDto(category,new CategoryResponseDto());
        }else{
            throw new Exception("Il n'y a pas de catégorie à ce nom");
        }
    }

    @Override
    public CategoryResponseDto create(CategoryRequestDto dto) throws Exception {
        if(dto.getName().length()>0){
            Category category = _dtoUtils.convertToEntity(new Category(),dto);
            return (CategoryResponseDto) _dtoUtils.convertToDto(_categoryRepository.save(category),new CategoryResponseDto());
        }else{
            throw new Exception("Le nom ne doit pas être nul");
        }
    }

    @Override
    public CategoryResponseDto update(CategoryRequestDto dto,long id) throws Exception {
        if (dto.getName()!=null){
            Optional<Category> category = _categoryRepository.findById(id);
            if(category.isPresent()){
                category.get().setName(dto.getName());
                return (CategoryResponseDto) _dtoUtils.convertToDto(_categoryRepository.save(category.get()),new CategoryResponseDto());
            }else{
                throw new Exception("L'Identifiant de la catégorie à modifier n'existe pas ");
            }
        }else{
            throw new Exception("Le nom de la catégorie ne doit pas être nul");
        }
    }

    @Override
    public String delete(long id) throws Exception {
       if(_categoryRepository.existsById(id)){
           _categoryRepository.deleteById(id);
           return "La catégorie a bien été supprimée";
       }else{
           throw new Exception("Il n'y a pas de catégorie à cet identifiant");
       }
    }

    @Override
    public CategoryResponseDto findById(long id) throws Exception {
       Optional<Category>optCat=_categoryRepository.findById(id);
       if(optCat.isPresent()){
           return (CategoryResponseDto) _dtoUtils.convertToDto(optCat.get(),new CategoryResponseDto());
       }else{
           throw new Exception("Il n'ya pas de catégorie à cet identifiant");
       }
    }

    @Override
    public List<CategoryResponseDto> findAll() throws Exception {
       List<Category> categories = (List<Category>) _categoryRepository.findAll();
       List<CategoryResponseDto>categoryDtos = new ArrayList<>();
       if(categories.size()>0){
           for(Category c:categories){
               CategoryResponseDto dto = (CategoryResponseDto) _dtoUtils.convertToDto(c,new CategoryResponseDto());
               categoryDtos.add(dto);
           }
           return categoryDtos;
       }else{
           throw new Exception("Il n'y a pas de catégorie");
       }
    }
}
