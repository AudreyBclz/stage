package com.example.productordermicroservice.service;

import com.example.productordermicroservice.dto.ProductRequestDto;
import com.example.productordermicroservice.dto.ProductResponseDto;

import java.util.List;

public interface ProductService {
    ProductResponseDto create(ProductRequestDto dto) throws Exception;
    ProductResponseDto update(ProductRequestDto dto,long id) throws Exception;
    List<ProductResponseDto>findByCategorieName(String name) throws Exception;
    List<ProductResponseDto>findAll()throws Exception;
    ProductResponseDto getOneById(long id)throws Exception;
    String delete(long id)throws Exception;
    List<ProductResponseDto>findAllStockNotNull()throws Exception;
}
