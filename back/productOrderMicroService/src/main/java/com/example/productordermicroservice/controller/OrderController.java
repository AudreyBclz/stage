package com.example.productordermicroservice.controller;

import com.example.productordermicroservice.dto.OrderRequestDto;
import com.example.productordermicroservice.dto.OrderResponseDto;
import com.example.productordermicroservice.enums.Status;
import com.example.productordermicroservice.service.impl.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/order")
@CrossOrigin(value = "http://localhost:8083",methods = {RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT,RequestMethod.PATCH})
public class OrderController {
    @Autowired
    private OrderServiceImpl _orderService;

    @PostMapping("")
    public ResponseEntity<OrderResponseDto>create(@RequestBody OrderRequestDto dto) throws Exception {
        return ResponseEntity.ok(_orderService.create(dto));
    }
    @PatchMapping("/status/{id}")
    public ResponseEntity<OrderResponseDto>paid(@PathVariable long id, @RequestBody OrderRequestDto dto)throws Exception{
        return ResponseEntity.ok(_orderService.updateStatus(dto, id));
    }
    @GetMapping("/user/{id}")
    public ResponseEntity<List<OrderResponseDto>>findOrderByUser(@PathVariable long id)throws Exception{
        return ResponseEntity.ok(_orderService.findOrderByUser(id));
    }
    @GetMapping("/{id}")
    public ResponseEntity<OrderResponseDto>findById(@PathVariable long id)throws Exception{
        return ResponseEntity.ok(_orderService.findOrderById(id));
    }
}
