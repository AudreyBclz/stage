package com.example.productordermicroservice.service;

import com.example.productordermicroservice.dto.CategoryRequestDto;
import com.example.productordermicroservice.dto.CategoryResponseDto;

import java.util.List;

public interface CategoryService {
    CategoryResponseDto findCatByName(String name) throws Exception;
    CategoryResponseDto create(CategoryRequestDto dto) throws Exception;
    CategoryResponseDto update(CategoryRequestDto dto,long id) throws Exception;
    String delete(long id)throws Exception;
    CategoryResponseDto findById(long id) throws Exception;
    List<CategoryResponseDto>findAll() throws Exception;
}
