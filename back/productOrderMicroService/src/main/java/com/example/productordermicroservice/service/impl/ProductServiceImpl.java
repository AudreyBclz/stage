package com.example.productordermicroservice.service.impl;

import com.example.productordermicroservice.dto.*;
import com.example.productordermicroservice.entity.Category;
import com.example.productordermicroservice.entity.Product;
import com.example.productordermicroservice.repository.ProductRepository;
import com.example.productordermicroservice.service.ProductService;
import com.example.productordermicroservice.tool.DtoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository _productRpository;
    @Autowired
    private DtoUtils<Product, ProductDto> _dtoUtils;
    @Autowired
    private CategoryServiceImpl _categoryService;
    @Autowired
    private DtoUtils<Category, CategoryDto> _dtoUtilsCat;
    @Override
    public ProductResponseDto create(ProductRequestDto dto) throws Exception {
        if(dto.getName()!=null && dto.getDescription()!=null && dto.getPrice()!=-1 & dto.getStock()!=-1 && dto.getCategory()!= null){
            Product product = _dtoUtils.convertToEntity(new Product(),dto);
            return (ProductResponseDto) _dtoUtils.convertToDto(_productRpository.save(product),new ProductResponseDto());
        }
        throw new Exception("Les champs nom, description, prix, stock et catégorie sont obligatoires");
    }

    @Override
    public ProductResponseDto update(ProductRequestDto dto, long id) throws Exception {
        if(dto.getName()!=null && dto.getDescription()!=null && dto.getPrice()!=-1 & dto.getStock()!=-1 && dto.getCategory()!= null){
            Optional<Product> product = _productRpository.findById(id);
            if(product.isPresent()){
                product.get().setName(dto.getName());
                product.get().setDescription(dto.getDescription());
                product.get().setPrice(dto.getPrice());
                product.get().setStock(dto.getStock());
                product.get().setCategory(dto.getCategory());
                return (ProductResponseDto) _dtoUtils.convertToDto(_productRpository.save(product.get()),new ProductResponseDto());
            }else{
                throw new Exception("L'identifiant du produit à modifier est incorrect");
            }
        }else{
            throw new Exception("Les champs nom, description, prix, stock et catégorie sont obligatoires");
        }
    }

    @Override
    public List<ProductResponseDto> findByCategorieName(String name) throws Exception {
        List<ProductResponseDto> productDtos = new ArrayList<>();
        CategoryResponseDto categoryResponseDto = _categoryService.findCatByName(name);
        if(categoryResponseDto!=null){
            Category category = _dtoUtilsCat.convertToEntity(new Category(),_categoryService.findCatByName(name));
            List<Product> products = _productRpository.findProductsByCategory(category);

            if(products.size()>0){
                for(Product p: products){
                    ProductResponseDto dto = (ProductResponseDto) _dtoUtils.convertToDto(p,new ProductResponseDto());
                    productDtos.add(dto);
                }
            }else{
                throw new Exception("Il n'y a pas de produit dans cette catégorie");
            }
        }else{
            throw new Exception("Cette catégorie n'existe pas");
        }
       return productDtos;
    }

    @Override
    public List<ProductResponseDto> findAll() throws Exception {
        List<Product> products = (List<Product>) _productRpository.findAll();
        List<ProductResponseDto> productDtos = new ArrayList<>();
        if(products.size()>0){
            for(Product p:products){
                ProductResponseDto dto = (ProductResponseDto) _dtoUtils.convertToDto(p,new ProductResponseDto());
                productDtos.add(dto);
            }
            return productDtos;
        }
        else{
            throw new Exception("Il n'y a pas de produits");
        }
    }

    @Override
    public ProductResponseDto getOneById(long id) throws Exception {
     Optional<Product> optProduct = _productRpository.findById(id);
     if(optProduct.isPresent()){
         return (ProductResponseDto) _dtoUtils.convertToDto(optProduct.get(),new ProductResponseDto());
     }else{
         return null;
     }
    }

    @Override
    public String delete(long id) throws Exception {
        if(_productRpository.existsById(id)){
            _productRpository.deleteById(id);
            return "Le produit a bien été supprimé";
        }else{
            throw new Exception("Il n'y a pas de produit à cet identifiant");
        }
    }

    @Override
    public List<ProductResponseDto> findAllStockNotNull() throws Exception {
        List<Product>products= (List<Product>) _productRpository.findAll();
        List<ProductResponseDto> dtos= new ArrayList<>();
        for(Product p:products){
            if(p.getStock()>0){
                ProductResponseDto dto= (ProductResponseDto) _dtoUtils.convertToDto(p,new ProductResponseDto());
                dtos.add(dto);
            }
        }
        if(dtos.size()>0){
            return dtos;
        }else{
            throw new Exception("Il n'y a pas de produits disponible à la vente");
        }
    }
}
