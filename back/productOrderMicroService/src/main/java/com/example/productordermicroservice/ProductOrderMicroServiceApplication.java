package com.example.productordermicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductOrderMicroServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductOrderMicroServiceApplication.class, args);
    }

}
