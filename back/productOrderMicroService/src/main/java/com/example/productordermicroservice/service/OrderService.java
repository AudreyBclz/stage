package com.example.productordermicroservice.service;

import com.example.productordermicroservice.dto.OrderRequestDto;
import com.example.productordermicroservice.dto.OrderResponseDto;
import com.example.productordermicroservice.enums.Status;

import java.util.List;

public interface OrderService {
    OrderResponseDto create(OrderRequestDto dto) throws Exception;
    OrderResponseDto updateStatus(OrderRequestDto dto,long id)throws Exception;
    List<OrderResponseDto> findOrderByUser(long idUser)throws Exception;
    OrderResponseDto findOrderById(long id)throws Exception;
}
