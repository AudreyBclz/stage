package com.example.productordermicroservice.controller;

import com.example.productordermicroservice.dto.CategoryRequestDto;
import com.example.productordermicroservice.dto.CategoryResponseDto;
import com.example.productordermicroservice.service.impl.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/category")
@CrossOrigin(value = "http://localhost:8083",methods = {RequestMethod.POST,RequestMethod.DELETE,
        RequestMethod.PUT,RequestMethod.GET})
public class CategoryController {
    @Autowired
    private CategoryServiceImpl _categoryService;
    @PostMapping("")
    public ResponseEntity<CategoryResponseDto>create(@RequestBody CategoryRequestDto dto) throws Exception {
        return ResponseEntity.ok(_categoryService.create(dto));
    }
    @PutMapping("/{id}")
    public ResponseEntity<CategoryResponseDto>update(@RequestBody CategoryRequestDto dto,@PathVariable long id)throws Exception{
        return ResponseEntity.ok(_categoryService.update(dto, id));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<String>delete(@PathVariable long id)throws Exception{
        return ResponseEntity.ok(_categoryService.delete(id));
    }
    @GetMapping("/{id}")
    public ResponseEntity<CategoryResponseDto>findById(@PathVariable long id)throws Exception{
        return ResponseEntity.ok(_categoryService.findById(id));
    }
    @GetMapping("")
    public ResponseEntity<List<CategoryResponseDto>>findAll()throws Exception{
        return ResponseEntity.ok(_categoryService.findAll());
    }
}
