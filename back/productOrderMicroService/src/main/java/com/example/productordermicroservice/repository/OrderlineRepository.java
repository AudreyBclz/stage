package com.example.productordermicroservice.repository;

import com.example.productordermicroservice.entity.Orderline;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderlineRepository extends CrudRepository<Orderline,Long> {
 @Query("select o from Orderline o where o.order.id=:id")
 List<Orderline>getOrderlineByOrder(long id);
}
