package com.example.productordermicroservice.controller;

import com.example.productordermicroservice.dto.ProductRequestDto;
import com.example.productordermicroservice.dto.ProductResponseDto;
import com.example.productordermicroservice.service.impl.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/product")
@CrossOrigin(value = "http://localhost:8083",methods = {RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class ProductController {

    @Autowired
    private ProductServiceImpl _productService;

    @PostMapping("")
    public ResponseEntity<ProductResponseDto>create(@RequestBody ProductRequestDto dto) throws Exception {
        return ResponseEntity.ok(_productService.create(dto));
    }
    @PutMapping("/{id}")
    public ResponseEntity<ProductResponseDto>update(@RequestBody ProductRequestDto dto,@PathVariable long id) throws Exception {
        return ResponseEntity.ok(_productService.update(dto, id));
    }
    @GetMapping("category/{category}")
    public ResponseEntity<List<ProductResponseDto>> findByCat(@PathVariable String category) throws Exception {
        return ResponseEntity.ok(_productService.findByCategorieName(category));
    }
    @GetMapping("")
    public ResponseEntity<List<ProductResponseDto>>findAll()throws Exception{
        return ResponseEntity.ok(_productService.findAll());
    }
    @GetMapping("/{id}")
    public ResponseEntity<ProductResponseDto>getById(@PathVariable long id)throws Exception{
        return ResponseEntity.ok(_productService.getOneById(id));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<String>delete(@PathVariable long id)throws Exception{
        return ResponseEntity.ok(_productService.delete(id));
    }
    @GetMapping("/dispo")
    public ResponseEntity<List<ProductResponseDto>>getAllProdAvailable()throws Exception{
        return ResponseEntity.ok(_productService.findAllStockNotNull());
    }

}
