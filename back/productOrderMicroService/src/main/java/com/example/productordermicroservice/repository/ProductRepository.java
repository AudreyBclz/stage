package com.example.productordermicroservice.repository;

import com.example.productordermicroservice.entity.Category;
import com.example.productordermicroservice.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product,Long> {
    List<Product> findProductsByCategory (Category category);
}
