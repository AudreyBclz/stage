package com.example.productordermicroservice.service;

import com.example.productordermicroservice.dto.OrderlineRequestDto;
import com.example.productordermicroservice.dto.OrderlineResponseDto;

import java.util.List;

public interface OrderlineService {
    OrderlineResponseDto create(OrderlineRequestDto dto) throws Exception;
    List<OrderlineResponseDto> selectByOrder(long idOrder)throws Exception;
}
