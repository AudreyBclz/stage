package com.example.productordermicroservice.dto;

import com.example.productordermicroservice.entity.Category;
import com.example.productordermicroservice.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductResponseDto implements ProductDto {

    private long id;
    private String name;
    private String description;
    private double price;
    private int stock;
    private Product sauce;
    private Category category;
}
