package com.example.productordermicroservice.tool;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class DtoUtils<T,V> {
    public V convertToDto(T entity, V dto){ return (V)new ModelMapper().map(entity,dto.getClass());}
    public T convertToEntity(T entity,V dto){ return (T)new ModelMapper().map(dto,entity.getClass());}
}
