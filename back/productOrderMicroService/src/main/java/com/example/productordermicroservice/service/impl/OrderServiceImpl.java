package com.example.productordermicroservice.service.impl;

import com.example.productordermicroservice.dto.*;
import com.example.productordermicroservice.entity.Orderline;
import com.example.productordermicroservice.entity.Orders;
import com.example.productordermicroservice.enums.Status;
import com.example.productordermicroservice.repository.OrderRepository;
import com.example.productordermicroservice.repository.OrderlineRepository;
import com.example.productordermicroservice.repository.ProductRepository;
import com.example.productordermicroservice.service.OrderService;
import com.example.productordermicroservice.tool.DtoUtils;
import com.fasterxml.uuid.Generators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private DtoUtils<Orders, OrderDto> _dtoUtils;
    @Autowired
    private OrderRepository _orderRepository;
    @Autowired
    private OrderlineRepository _orderlineRepository;
    @Autowired
    private ProductRepository _productRepository;
    @Override
    public OrderResponseDto create(OrderRequestDto dto) throws Exception {
        if(dto.getUser()!=0){
           try{
               Orders order= new Orders();
               order.setUser(dto.getUser());
               order.setDate(new Date());
               order.setStatus(Status.INPROGRESS);
               order.setNumOrder(Generators.randomBasedGenerator().generate().toString());
               Orders orderSaved =_orderRepository.save(order);
               for(ProductCart prodCart : dto.getProductCarts()){
                   Orderline orderline = new Orderline();
                   orderline.setQuantity(prodCart.getQuantity());
                   if(Objects.equals(prodCart.getProduct().getCategory().getName(), "Sauce") || Objects.equals(prodCart.getProduct().getCategory().getName(), "Boissons" )){
                       orderline.setNameProduct(prodCart.getProduct().getCategory().getName()+" "+prodCart.getProduct().getName());
                   }else {
                       orderline.setNameProduct(prodCart.getProduct().getCategory().getName()+" "+ prodCart.getProduct().getName()+" "+prodCart.getProduct().getSauce().getDescription());
                   }
                   orderline.setPrice(prodCart.getProduct().getPrice());
                   orderline.setProduct(prodCart.getProduct());
                   orderline.setOrder(order);
                   prodCart.getProduct().setStock(prodCart.getProduct().getStock()- prodCart.getQuantity());
                   _productRepository.save(prodCart.getProduct());
                   orderSaved.getOrderlines().add(_orderlineRepository.save(orderline));
               }
               return new OrderResponseDto(order.getId(), order.getDate(), order.getUser(), order.getNumOrder(),order.getStatus(),orderSaved.getOrderlines());
           }catch (Exception ex) {
               throw new Exception("Erreur dans la création de la commande");
           }
        }else{
            throw new Exception("La date et l'utilisateur sont obligatoires");
        }
    }

    @Override
    public OrderResponseDto updateStatus(OrderRequestDto dto,long id) throws Exception {
        Optional<Orders> order = _orderRepository.findById(id);
        if(order.isPresent()){
            order.get().setStatus(dto.getStatus());
            return (OrderResponseDto) _dtoUtils.convertToDto(_orderRepository.save(order.get()),new OrderResponseDto());
        }else{
            throw new Exception("Il n'y a pas de commande à cet identifiant");
        }

    }

    @Override
    public List<OrderResponseDto> findOrderByUser(long idUser) throws Exception {
        List<Orders> ordersList = _orderRepository.findOrdersByUser(idUser);
        List<OrderResponseDto>dtos=new ArrayList<>();
        if (ordersList.size() > 0) {
            for(Orders o: ordersList){
                OrderResponseDto dto = (OrderResponseDto) _dtoUtils.convertToDto(o,new OrderResponseDto());
                dtos.add(dto);
            }
            return dtos;
        }else{
            throw new Exception("Il n'y a pas de commandes passées par l'utilisateur");
        }
    }

    @Override
    public OrderResponseDto findOrderById(long id) throws Exception {
        Optional<Orders> optOrder = _orderRepository.findById(id);
        if(optOrder.isPresent()){
            return (OrderResponseDto) _dtoUtils.convertToDto(optOrder.get(),new OrderResponseDto());
        }else{
            throw new Exception("Il n' y a pas de commande à cet identifiant");
        }
    }
}
