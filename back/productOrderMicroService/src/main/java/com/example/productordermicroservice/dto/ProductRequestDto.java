package com.example.productordermicroservice.dto;

import com.example.productordermicroservice.entity.Category;
import com.example.productordermicroservice.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequestDto implements ProductDto {

    private String name;
    private String description;
    private double price;
    private int stock;
    private Product sauce;
    private Category category;
}
