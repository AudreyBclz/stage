package com.example.productordermicroservice.repository;

import com.example.productordermicroservice.entity.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category,Long> {
    Category findCategoryByName(String name);
}
