package com.example.productordermicroservice.repository;

import com.example.productordermicroservice.entity.Orders;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Orders,Long> {
    List<Orders> findOrdersByUser(long id);
}
