package com.example.productordermicroservice.dto;

import com.example.productordermicroservice.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequestDto implements OrderDto {
    private Date date;
    private long user;
    private ProductCart[] productCarts;
    private Status status;
}
