package com.example.productordermicroservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    @Column(length = 1000)
    private String description;

    private double price;
    private int stock;
    @OneToOne
    @JoinColumn(name = "id_sauce",nullable = true)
    @Transient
    private Product sauce;

    @ManyToOne
    @JoinColumn(name = "id_c")
    private Category category;

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private List<Orderline> orderlines;

}
