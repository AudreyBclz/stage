package com.example.productordermicroservice.dto;

import com.example.productordermicroservice.entity.Orderline;
import com.example.productordermicroservice.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderResponseDto implements OrderDto {
    private long id;
    private Date date;
    private long user;
    private String numOrder;
    private Status status;
    private List<Orderline> orderlines;
}
