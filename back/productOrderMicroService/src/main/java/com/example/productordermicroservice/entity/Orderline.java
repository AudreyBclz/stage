package com.example.productordermicroservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Orderline {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int quantity;
    @Column(name = "product")
    private String nameProduct;
    private double price;
    @ManyToOne
    @JoinColumn(name = "id_p")
    private Product product;
    @ManyToOne
    @JoinColumn(name = "id_o")
    @JsonIgnore
    private Orders order;

}
