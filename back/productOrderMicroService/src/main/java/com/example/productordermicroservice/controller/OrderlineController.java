package com.example.productordermicroservice.controller;

import com.example.productordermicroservice.dto.OrderlineRequestDto;
import com.example.productordermicroservice.dto.OrderlineResponseDto;
import com.example.productordermicroservice.service.impl.OrderlineServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("api/orderline")
@CrossOrigin(value = "http://localhost:8083",methods = {RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class OrderlineController {
    @Autowired
    private OrderlineServiceImpl _orderlineService;
    @PostMapping("")
    public ResponseEntity<OrderlineResponseDto>create(@RequestBody OrderlineRequestDto dto) throws Exception {
        return ResponseEntity.ok(_orderlineService.create(dto));
    }

    @GetMapping("/{idOrder}/order")
    public ResponseEntity<List<OrderlineResponseDto>>findByOrder(@PathVariable long idOrder)throws Exception{
        return ResponseEntity.ok(_orderlineService.selectByOrder(idOrder));

    }

}
