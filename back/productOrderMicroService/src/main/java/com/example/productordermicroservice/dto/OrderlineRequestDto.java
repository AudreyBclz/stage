package com.example.productordermicroservice.dto;

import com.example.productordermicroservice.entity.Orders;
import com.example.productordermicroservice.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderlineRequestDto implements OrderlineDto {
    private int quantity;
    private String nameProduct;
    private double price;

    private Product product;

    private Orders order;
}
