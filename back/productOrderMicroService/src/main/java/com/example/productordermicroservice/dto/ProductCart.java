package com.example.productordermicroservice.dto;

import com.example.productordermicroservice.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductCart {
    int quantity;
    Product product;

}
