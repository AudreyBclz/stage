package com.example.productordermicroservice.service.impl;

import com.example.productordermicroservice.dto.OrderlineDto;
import com.example.productordermicroservice.dto.OrderlineRequestDto;
import com.example.productordermicroservice.dto.OrderlineResponseDto;
import com.example.productordermicroservice.entity.Orderline;
import com.example.productordermicroservice.repository.OrderlineRepository;
import com.example.productordermicroservice.service.OrderlineService;
import com.example.productordermicroservice.tool.DtoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderlineServiceImpl implements OrderlineService {
    @Autowired
    private DtoUtils<Orderline, OrderlineDto>_dtoUtils;
    @Autowired
    private OrderlineRepository _orderlineRepository;
    @Override
    public OrderlineResponseDto create(OrderlineRequestDto dto) throws Exception {
       if (dto.getNameProduct()!=null && dto.getProduct()!=null && dto.getOrder()!=null && dto.getPrice()!=0 && dto.getQuantity()!=0){
           Orderline orderline = _dtoUtils.convertToEntity(new Orderline(),dto);
           return (OrderlineResponseDto) _dtoUtils.convertToDto(_orderlineRepository.save(orderline),new OrderlineResponseDto());
       }else{
           throw new Exception("Un des champs est manquant");
       }
    }

    @Override
    public List<OrderlineResponseDto> selectByOrder(long idOrder) throws Exception {
        List<Orderline> orderlines = _orderlineRepository.getOrderlineByOrder(idOrder);
        List<OrderlineResponseDto> dtos = new ArrayList<>();
        if(orderlines.size()>0){
            for(Orderline o:orderlines){
                OrderlineResponseDto dto = (OrderlineResponseDto) _dtoUtils.convertToDto(o,new OrderlineResponseDto());
                dtos.add(dto);
            }
            return dtos;
        }else{
            throw new Exception("Il n'y a pas de ligne de commande à cet identifiant de commande");
        }
    }
}
